//
// Created by Vince.Chen on 2017/11/29.
//

#ifndef GPIO_CTRL_JNI_H
#define GPIO_CTRL_JNI_H
#include <jni.h>
#include <android/log.h>
#include <android/bitmap.h>

#include <string.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include <fcntl.h>              /* low-level i/o */
#include <unistd.h>
#include <errno.h>
#include <malloc.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/mman.h>
#include <sys/ioctl.h>

#include <asm/types.h>          /* for videodev2.h */

#include <linux/videodev2.h>
#include <linux/usbdevice_fs.h>

#define  LOG_TAG    "TEST"
#define  LOGI(...)  __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)
#define  LOGE(...)  __android_log_print(ANDROID_LOG_ERROR,LOG_TAG,__VA_ARGS__)

#define CLEAR(x) memset (&(x), 0, sizeof (x))


struct GPIO_Reg
{
    unsigned int u32Reg;
    unsigned char u8Enable;
    unsigned char u8BitMsk;
} __attribute__ ((packed));

typedef struct GPIO_Reg GPIO_Reg_t;

#define ERROR_LOCAL -1
#define SUCCESS_LOCAL 0

#define GPIO_IOC_MAGIC               'g'

#define MDRV_GPIO_INIT               _IO(GPIO_IOC_MAGIC, 0)
#define MDRV_GPIO_SET                _IOW(GPIO_IOC_MAGIC, 1, unsigned char)
#define MDRV_GPIO_OEN                _IOW(GPIO_IOC_MAGIC, 2, unsigned char)
#define MDRV_GPIO_ODN                _IOW(GPIO_IOC_MAGIC, 3, unsigned char)
#define MDRV_GPIO_READ               _IOWR(GPIO_IOC_MAGIC, 4, unsigned char)
#define MDRV_GPIO_PULL_HIGH          _IOW(GPIO_IOC_MAGIC, 5, unsigned char)
#define MDRV_GPIO_PULL_LOW           _IOW(GPIO_IOC_MAGIC, 6, unsigned char)
#define MDRV_GPIO_INOUT              _IOWR(GPIO_IOC_MAGIC, 7, unsigned char)
#define MDRV_GPIO_WREGB              _IOW(GPIO_IOC_MAGIC, 8, GPIO_Reg_t)
#define KERN_GPIO_OEN                _IOW(GPIO_IOC_MAGIC, 9, unsigned char)
#define KERN_GPIO_PULL_HIGH          _IOW(GPIO_IOC_MAGIC, 10, unsigned char)
#define KERN_GPIO_PULL_LOW           _IOW(GPIO_IOC_MAGIC, 11, unsigned char)

#define GPIO_IOC_MAXNR               12


static unsigned long   index=0;
static int              gpiofd              = -1;

//int RebootSys();
void Java_com_ss_android_1ss_1iris_1user_1app_module_CtrlDevModule_RebootSys( JNIEnv* env,jobject thiz);
void Java_com_ss_android_1ss_1iris_1user_1app_module_CtrlDevModule_OpenGPIODevice( JNIEnv* env,jobject thiz);
void Java_com_ss_android_1ss_1iris_1user_1app_module_CtrlDevModule_SetGPIOOutHigh( JNIEnv* env, jobject thiz, jint pin);
void Java_com_ss_android_1ss_1iris_1user_1app_module_CtrlDevModule_SetGPIOOutLow( JNIEnv* env, jobject thiz, jint pin);
jint Java_com_ss_android_1ss_1iris_1user_1app_module_CtrlDevModule_GetGPIO( JNIEnv* env, jobject thiz, jint pin);
jint Java_com_ss_android_1ss_1iris_1user_1app_module_CtrlDevModule_SetGPIOInOut( JNIEnv* env, jobject thiz, jint pin,jint inout);
#endif //GPIO_CTRL_JNI_H
