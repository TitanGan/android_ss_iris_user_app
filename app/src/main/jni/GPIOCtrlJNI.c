#include "GPIOCtrlJNI.h"
#include "ioctl.h"
//
// Created by Vince.Chen on 2017/11/29.
//

int RebootSys()
{
    char system_call_buffer[100];
    sprintf(system_call_buffer,"reboot");

    int ret = system(system_call_buffer);

    if (ret !=0) {
        LOGE("Could not %s : %d, %s",system_call_buffer, errno, strerror (errno));
        return ERROR_LOCAL;
    }
    return SUCCESS_LOCAL;
}

int OpenGPIODevice() {
    if (gpiofd == -1) {
        gpiofd = open("/dev/Mstar-gpio", O_RDWR);
        LOGE("open gpiofd = %d", gpiofd);
        if (gpiofd < 0) {
            LOGE("open %s failed\n", gpiofd);
            return -1;
        }
    } else
        LOGE("gpio driver have been opened\n", gpiofd);
    return SUCCESS_LOCAL;
}

int CloseGPIODevice(){
    if (gpiofd == -1){
        close(gpiofd);
    }
    return SUCCESS_LOCAL;
}

int SetGPIOOutHigh(unsigned long pin) {
    index = pin;
    if (ioctl(gpiofd, MDRV_GPIO_PULL_HIGH, &index)<0)
    {
        LOGE("set MDRV_GPIO_PULL_HIGH failed");
        return -1;
    }
    return SUCCESS_LOCAL;
}

int SetGPIOOutLow(unsigned long pin) {
    index = pin;
    if (ioctl(gpiofd, MDRV_GPIO_PULL_LOW, &index)<0)
    {
        LOGE("set MDRV_GPIO_PULL_LOW failed");
        return -1;
    }
    return SUCCESS_LOCAL;
}

int GetGPIO(unsigned long pin)
{
    index = pin;
    if (ioctl(gpiofd, MDRV_GPIO_READ, &index)<0)
    {
        LOGE("set MDRV_GPIO_READ failed");
        return -1;
    }
    LOGE("GPIO value = %d",index);
    return index;
}

int SetGPIOInOut(unsigned long pin,unsigned int inout)
{
    index = pin;
    //set gpio input or output--OEN: output ODN:input
    if(inout) {
        if (ioctl(gpiofd, MDRV_GPIO_OEN, &index) < 0) {
            LOGE("set MDRV_GPIO_OEN failed");
            return -1;
        }
    } else
    {
        if (ioctl(gpiofd, MDRV_GPIO_ODN, &index) < 0) {
            LOGE("set MDRV_GPIO_ODN failed");
            return -1;
        }
    }
    return SUCCESS_LOCAL;
}

void
Java_com_ss_android_1ss_1iris_1user_1app_module_CtrlDevModule_RebootSys( JNIEnv* env, jobject thiz){
    RebootSys();
}

void
Java_com_ss_android_1ss_1iris_1user_1app_module_CtrlDevModule_OpenGPIODevice( JNIEnv* env, jobject thiz){
    OpenGPIODevice();
}

void
Java_com_ss_android_1ss_1iris_1user_1app_module_CtrlDevModule_CloseGPIODevice( JNIEnv* env, jobject thiz){
    CloseGPIODevice();
}

void
Java_com_ss_android_1ss_1iris_1user_1app_module_CtrlDevModule_SetGPIOOutHigh( JNIEnv* env, jobject thiz, jint pin){
    SetGPIOOutHigh(pin);
}


void
Java_com_ss_android_1ss_1iris_1user_1app_module_CtrlDevModule_SetGPIOOutLow( JNIEnv* env, jobject thiz, jint pin){
    SetGPIOOutLow(pin);
}

jint
Java_com_ss_android_1ss_1iris_1user_1app_module_CtrlDevModule_GetGPIO( JNIEnv* env, jobject thiz, jint pin){
    return GetGPIO(pin);
}

jint
Java_com_ss_android_1ss_1iris_1user_1app_module_CtrlDevModule_SetGPIOInOut( JNIEnv* env, jobject thiz, jint pin, jint inout){
    SetGPIOInOut(pin,inout);
}