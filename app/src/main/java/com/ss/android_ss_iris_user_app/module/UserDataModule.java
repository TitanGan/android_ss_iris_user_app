package com.ss.android_ss_iris_user_app.module;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.ss.android_ss_iris_user_app.model.UserData;

import java.util.ArrayList;

/**
 * Created by TitanGan on 2017/12/11.
 */

public class UserDataModule extends SQLiteOpenHelper {

    private static final String LOG_TAG = "UserDataModule";

    // db configuration0
    private final static String TABLE_NAME = "data";

    // db table column name
    private final static String KEY_ID = "_id";
    private final static String KEY_INFO = "_info";
    private final static String KEY_VALUE = "_value";
    private final static String KEY_KIND = "_kind";

    // db table column index
    private final static int TABLE_COLUMN_ID = 0;
    private final static int TABLE_COLUMN_INFO = 1;
    private final static int TABLE_COLUMN_VALUE = 2;
    private final static int TABLE_COLUMN_KIND = 3;

    // clear and reset db for debuging
    private final static boolean RESET_DB = false;

    public UserDataModule(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        final String sql = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "( " + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + KEY_INFO + " INTEGER NOT NULL, "
                + KEY_VALUE + " TEXT NOT NULL, "
                + KEY_KIND + " INTEGER NOT NULL"
                + ");";
        Log.d(LOG_TAG, "onCreate sql: " + sql);

        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        final String sql = "DROP TABLE " + TABLE_NAME;
        Log.d(LOG_TAG, "onUpgrade sql: " + sql);
        db.execSQL(sql);
        onCreate(db);
    }

    synchronized public long insert(UserData data) {
        SQLiteDatabase db = getWritableDatabase();
        if (RESET_DB)
            onUpgrade(db, 0, 1);

        ContentValues values = new ContentValues();
        values.put(KEY_INFO, data.info);
        values.put(KEY_VALUE, data.value);
        values.put(KEY_KIND, data.kind);
        long rowId = db.insert(TABLE_NAME, null, values);
        db.close();
        return rowId;
    }

    synchronized public ArrayList<UserData> fetchAll() {
        SQLiteDatabase db = getReadableDatabase();
        Cursor c = db.query(TABLE_NAME, new String[]{KEY_ID, KEY_INFO, KEY_VALUE, KEY_KIND}, null, null, null, null, null);
        if (c == null || c.getCount() <= 0) {
            closeDb(db, c);
            return null;
        }

        c.moveToFirst();
        ArrayList<UserData> dataList = new ArrayList<UserData>();
        for (int i = 0; i < c.getCount(); i++) {
            UserData data = new UserData(c.getInt(TABLE_COLUMN_ID), c.getString(TABLE_COLUMN_INFO), c.getString(TABLE_COLUMN_VALUE), c.getInt(TABLE_COLUMN_KIND));
            dataList.add(data);
            c.moveToNext();
        }
        closeDb(db, c);
        return dataList;
    }

    synchronized public UserData fetch(int id) {
        SQLiteDatabase db = getReadableDatabase();
        Cursor c = db.query(TABLE_NAME, new String[] { KEY_ID, KEY_INFO, KEY_VALUE, KEY_KIND}, KEY_ID + "=" + id, null, null, null, null);
        if (c == null || c.getCount() <= 0) {
            closeDb(db, c);
            return null;
        }

        c.moveToFirst();
        UserData data = new UserData(c.getInt(TABLE_COLUMN_ID), c.getString(TABLE_COLUMN_INFO), c.getString(TABLE_COLUMN_VALUE), c.getInt(TABLE_COLUMN_KIND));
        closeDb(db, c);
        return data;
    }

    synchronized public UserData fetchFirstByInfo(String info) {
        SQLiteDatabase db = getReadableDatabase();
        Cursor c = db.query(TABLE_NAME, new String[] { KEY_ID, KEY_INFO, KEY_VALUE, KEY_KIND}, KEY_INFO + "=\"" + info + "\"", null, null, null, null);
        if (c == null || c.getCount() <= 0) {
            closeDb(db, c);
            return null;
        }

        try {
            c.moveToFirst();
        } catch(Exception e) {
            e.printStackTrace();
            closeDb(db, c);
            return null;
        }

        UserData data = new UserData(c.getInt(TABLE_COLUMN_ID), c.getString(TABLE_COLUMN_INFO), c.getString(TABLE_COLUMN_VALUE), c.getInt(TABLE_COLUMN_KIND));
        closeDb(db, c);
        return data;
    }

    synchronized public ArrayList<UserData> fetchByInfo(String info) {
        SQLiteDatabase db = getReadableDatabase();
        Cursor c = db.query(TABLE_NAME, new String[] { KEY_ID, KEY_INFO, KEY_VALUE, KEY_KIND}, KEY_INFO + "=\"" + info + "\"", null, null, null, null);
        if (c == null || c.getCount() <= 0) {
            closeDb(db, c);
            return new ArrayList<UserData>();
        }

        try {
            c.moveToFirst();
        } catch(Exception e) {
            e.printStackTrace();
            closeDb(db, c);
            return new ArrayList<UserData>();
        }

        ArrayList<UserData> dataList = new ArrayList<UserData>();
        for (int i = 0; i < c.getCount(); i++) {
            UserData data = new UserData(c.getInt(TABLE_COLUMN_ID), c.getString(TABLE_COLUMN_INFO), c.getString(TABLE_COLUMN_VALUE), c.getInt(TABLE_COLUMN_KIND));
            dataList.add(data);
            c.moveToNext();
        }
        closeDb(db, c);
        return dataList;
    }

    synchronized public ArrayList<UserData> fetchByKind(int kind) {
        SQLiteDatabase db = getReadableDatabase();
        Cursor c = db.query(TABLE_NAME, new String[] { KEY_ID, KEY_INFO, KEY_VALUE, KEY_KIND}, KEY_KIND + "=\"" + kind + "\"", null, null, null, null);
        if (c == null || c.getCount() <= 0) {
            closeDb(db, c);
            return new ArrayList<UserData>();
        }

        try {
            c.moveToFirst();
        } catch(Exception e) {
            e.printStackTrace();
            closeDb(db, c);
            return new ArrayList<UserData>();
        }

        ArrayList<UserData> dataList = new ArrayList<UserData>();
        for (int i = 0; i < c.getCount(); i++) {
            UserData data = new UserData(c.getInt(TABLE_COLUMN_ID), c.getString(TABLE_COLUMN_INFO), c.getString(TABLE_COLUMN_VALUE), c.getInt(TABLE_COLUMN_KIND));
            dataList.add(data);
            c.moveToNext();
        }
        closeDb(db, c);
        return dataList;
    }

    synchronized public ArrayList<String> fetchStringsByKind(int kind) {
        SQLiteDatabase db = getReadableDatabase();
        Cursor c = db.query(TABLE_NAME, new String[] { KEY_ID, KEY_INFO, KEY_VALUE, KEY_KIND}, KEY_KIND + "=\"" + kind + "\"", null, null, null, null);
        if (c == null || c.getCount() <= 0) {
            closeDb(db, c);
            return new ArrayList<String>();
        }

        try {
            c.moveToFirst();
        } catch(Exception e) {
            e.printStackTrace();
            closeDb(db, c);
            return new ArrayList<String>();
        }

        ArrayList<String> strings = new ArrayList<String>();
        for (int i = 0; i < c.getCount(); i++) {
            String string = new String(c.getString(TABLE_COLUMN_INFO));
            strings.add(string);
            c.moveToNext();
        }
        closeDb(db, c);
        return strings;
    }

    synchronized public int deleteAll() {
        SQLiteDatabase db = getReadableDatabase();
        int ret = db.delete(TABLE_NAME, null, null);
        db.close();
        return ret;
    }

    synchronized public int delete(int id) {
        SQLiteDatabase db = getReadableDatabase();
        int ret = db.delete(TABLE_NAME, KEY_ID + "=" + id, null);
        db.close();
        return ret;
    }

    synchronized public int deleteByInfo(String info) {
        SQLiteDatabase db = getReadableDatabase();
        int ret = db.delete(TABLE_NAME, KEY_INFO + "=\"" + info + "\"", null);
        db.close();
        return ret;
    }

    synchronized public long update(UserData data) {
        deleteByInfo(data.info);
        return insert(data);
    }

    private void closeDb(SQLiteDatabase db, Cursor cursor) {
        if (cursor != null)
            cursor.close();

        if (db != null)
            db.close();
    }
}
