package com.ss.android_ss_iris_user_app.module;

import android.util.Log;

/**
 * Created by Vince.Chen on 2017/11/28.
 */

public class CtrlDevModule {
    public enum ExDevice {DOOR, LED, FAN, MOTOR};
    public enum ExDeviceBehavior {BEHA_ON, BEHA_OFF};
    private static String msgLog;
    private static String msgDevice;

    // JNI functions
    public native int RebootSys();
    public native int OpenGPIODevice();
    public native int CloseGPIODevice();
    public native void SetGPIOOutHigh(int pin);
    public native void SetGPIOOutLow(int pin);
    public native int GetGPIO(int pin);
    public native int SetGPIOInOut(int pin, int inout);
    static {
        System.loadLibrary("GPIOCtrlJNI");
    }

    //CtlModule Constructor
    public CtrlDevModule() {
        msgLog="Default msg";
        msgDevice="";
        OpenGPIODevice();
    }
    //moduleCmdParser method
    private void moduleCmdParser(){
        //TODO
    }

    public void setMsgLog(String str){
        msgLog = str;
    }

    public String getMsgLog(){
        return msgLog;
    }

    //This method For check external devices work well or not.
    public void ckExDevice(){
        //TODO
        Log.i("CtlModule","Check devices work well or not");
        //ex check door/LED open and close
        msgLog = "device is checking...";
        //TODO implement check device process.
    }

    public void ctlDevice(ExDevice dev,ExDeviceBehavior behavior){
        int testValue=-1;
        //TODO control external device
        switch(dev)
        {
            case DOOR:
                if(behavior == ExDeviceBehavior.BEHA_ON) {
                    Log.i("CtlModule", "Control door open");
                    msgLog = "Door open...";
                    //TODO control device
                    SetGPIOOutLow(48);
                }
                if(behavior == ExDeviceBehavior.BEHA_OFF) {
                    Log.i("CtlModule", "Control door close");
                    msgLog = "Door close...";
                    //TODO control device
                    SetGPIOOutHigh(48);
                }
                break;
            case LED:
                if(behavior == ExDeviceBehavior.BEHA_ON) {
                    Log.i("CtlModule", "Control led on");
                    msgLog = "LED on...";
                    //TODO control device
                    //test get gpio
                    //testValue = GetGPIO(18);
                    //Log.i("CtlModule", "gpio value ="+testValue);
                    //SetGPIOInOut(48,0);
                }
                if(behavior == ExDeviceBehavior.BEHA_OFF) {
                    Log.i("CtlModule", "Control led off");
                    msgLog = "LED off...";
                    //TODO control device
                    //test set gpio input output
                    //SetGPIOInOut(48,1);

                }
                break;
            case FAN:
                Log.i("CtlModule","Control fan");
                msgLog = "control fan...";
                //TODO control device
                break;
            case MOTOR:
                Log.i("CtlModule","Control motor");
                msgLog = "control motor...";
                //TODO control device
                break;
        }
    }

    public String moduleUIStrMssage(){
        //TODO ex: link android toast object...
        return msgLog;
    }
}