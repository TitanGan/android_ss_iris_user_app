package com.ss.android_ss_iris_user_app;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import android.widget.ImageView;
import android.widget.ProgressBar;

import com.ss.android_ss_iris_user_app.ListAdapter.UserFragmentPagerAdapter;
import com.ss.android_ss_iris_user_app.comm.GetAutomaticDismantleListHttpOperation;
import com.ss.android_ss_iris_user_app.comm.bk.GetControlHttpOperation;
import com.ss.android_ss_iris_user_app.comm.op.CommCompletionHandler;
import com.ss.android_ss_iris_user_app.comm.op.CommError;
import com.ss.android_ss_iris_user_app.generic.InspectControlDoorDatabase;
import com.ss.android_ss_iris_user_app.model.ControlDoorItem;
import com.ss.android_ss_iris_user_app.model.DismantleItem;
import com.ss.android_ss_iris_user_app.ui.AppUncaughtExceptionHandler;
import com.ss.android_ss_iris_user_app.ui.Util;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private UserFragmentPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;
    private ProgressBar progressBarIris, progressBarRfid, progressBarUser, progressBarFace;
    private ImageView imageViewIris, imageViewRfid, imageViewUser, imageViewFace;
    private DismantleItem dismantleItem;
    private ArrayList<String> mImagePaths = new ArrayList<String>();
    private MainActivity parent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        S.httpCommunicationManager.setActivity(this);
        Thread.setDefaultUncaughtExceptionHandler(new AppUncaughtExceptionHandler(this));
        parent = this;

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setLogo(R.drawable.icon_logo);

//        final List<Fragment> fragmentList = new ArrayList<Fragment>();
//        fragmentList.add(new IrisFragment());
//        fragmentList.add(new FaceFragment());
//        fragmentList.add(new RfidFragment());
//        fragmentList.add(new InterFragment());
//        mSectionsPagerAdapter = new UserFragmentPagerAdapter(getSupportFragmentManager(), fragmentList);
//        mViewPager = (ViewPager) findViewById(R.id.container);
//        mViewPager.setAdapter(mSectionsPagerAdapter);
//        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
//            @Override
//            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
//
//            }
//
//            @Override
//            public void onPageSelected(int position) {
//                setToolButton(position);
//            }
//
//            @Override
//            public void onPageScrollStateChanged(int state) {
//
//            }
//        });

        imageViewIris = (ImageView) findViewById(R.id.img_table_iris);
        imageViewIris.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //mViewPager.setCurrentItem(0, false);
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.container, IrisFragment.newInstance())
                        .commit();
                setToolButton(0);
            }
        });
        imageViewFace = (ImageView) findViewById(R.id.img_table_face);
        imageViewFace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //mViewPager.setCurrentItem(1, false);
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.container, FaceFragment.newInstance())
                        .commit();
                setToolButton(1);
            }
        });
        imageViewRfid = (ImageView) findViewById(R.id.img_table_rfid);
        imageViewRfid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //mViewPager.setCurrentItem(2, false);
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.container, RfidFragment.newInstance())
                        .commit();
                setToolButton(2);
            }
        });
        imageViewUser = (ImageView) findViewById(R.id.img_table_user);
        imageViewUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //mViewPager.setCurrentItem(3, false);
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.container, InterFragment.newInstance(parent))
                        .commit();
                setToolButton(3);
            }
        });

        progressBarIris = (ProgressBar) findViewById(R.id.progressBar_iris);
        progressBarRfid = (ProgressBar) findViewById(R.id.progressBar_rfid);
        progressBarUser = (ProgressBar) findViewById(R.id.progressBar_user);
        progressBarFace = (ProgressBar) findViewById(R.id.progressBar_face);

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, IrisFragment.newInstance())
                .commit();

        setToolButton(0);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings_door_a) {
            S.DOOR = "doorA";
            return true;
        }
        else if (id == R.id.action_settings_door_b) {
            S.DOOR = "doorB";
            return true;
        }
        else if (id == R.id.action_settings_door_c) {
            S.DOOR = "doorC";
            return true;
        }
        else if (id == R.id.action_settings_door_d) {
            S.DOOR = "doorD";
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void setToolButton(int position){
        if (position == 0){
            progressBarIris.setVisibility(View.VISIBLE);
            progressBarFace.setVisibility(View.GONE);
            progressBarRfid.setVisibility(View.GONE);
            progressBarUser.setVisibility(View.GONE);
        }
        else if (position == 1){
            progressBarIris.setVisibility(View.GONE);
            progressBarFace.setVisibility(View.VISIBLE);
            progressBarRfid.setVisibility(View.GONE);
            progressBarUser.setVisibility(View.GONE);
        }else if (position == 2) {
            progressBarIris.setVisibility(View.GONE);
            progressBarFace.setVisibility(View.GONE);
            progressBarRfid.setVisibility(View.VISIBLE);
            progressBarUser.setVisibility(View.GONE);
        }
        else {
            progressBarIris.setVisibility(View.GONE);
            progressBarFace.setVisibility(View.GONE);
            progressBarRfid.setVisibility(View.GONE);
            progressBarUser.setVisibility(View.VISIBLE);
        }
    }

    private void queryData() {
        new GetAutomaticDismantleListHttpOperation(S.httpCommunicationManager,
                new CommCompletionHandler<DismantleItem>() {
                    @Override
                    public boolean onDataReceived(CommError error, int progress) {
                        return true;
                    }

                    @Override
                    public boolean onDataSent(CommError error, int progress) {
                        return true;
                    }

                    @Override
                    public boolean onCompletion(CommError error, DismantleItem result, String response) {
                        if (error != null) {
                            return true; // can't do anything with error
                        }
                        dismantleItem = result;
                        return true;
                    }
                });
    }

    public ArrayList<String> getmImagePaths(){
        return mImagePaths;
    }

}