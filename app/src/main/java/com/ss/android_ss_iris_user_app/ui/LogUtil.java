package com.ss.android_ss_iris_user_app.ui;

import android.util.Log;

import java.util.ArrayList;

/**
 * Created by TitanGan on 2017/12/19.
 */

public class LogUtil {
    private static boolean DEBUG = true;
    public static void i(Class<?> c, int line, String tag, String msg) {
        if (!DEBUG)
            return;

        ArrayList<String> messages = getMessage(c, line, msg);
        if (messages == null)
            return;

        for (String str : messages) {
            Log.i(tag, str);
        }
    }
    public static void v(Class<?> c, int line, String tag, String msg) {
        if (!DEBUG)
            return;

        ArrayList<String>messages = getMessage(c, line, msg);
        if (messages == null)
            return;

        for (String str : messages) {
            Log.v(tag, str);
        }
    }
    public static void d(Class<?> c, int line, String tag, String msg) {
        if (!DEBUG)
            return;

        ArrayList<String>messages = getMessage(c, line, msg);
        if (messages == null)
            return;

        for (String str : messages) {
            Log.d(tag, str);
        }
    }

    public static void e(Class<?> c, int line, String tag, String msg) {
        if (!DEBUG)
            return;

        ArrayList<String>messages = getMessage(c, line, msg);
        if (messages == null)
            return;

        for (String str : messages) {
            Log.e(tag, str);
        }
    }

    public static void dumpMemoryInfo(Class<?> c, int line, String tag, String msg) {
        System.gc();
        final double MEGA = 1048576.0;
        double freeSize = 0L;
        double totalSize = 0L;
        double usedSize = -1L;
        double maxHeapSizeInMB = 0L;
        try {
            Runtime info = Runtime.getRuntime();
            freeSize = info.freeMemory() / MEGA;
            totalSize = info.totalMemory() / MEGA;
            usedSize = totalSize - freeSize;
            maxHeapSizeInMB = info.maxMemory() / MEGA;
        } catch (Exception e) {
            e.printStackTrace();
        }

        String usage = String.format("Memory(%s):\ttotal:\t%f\tfree:\t%f\tused:\t%f\tmax:\t%f",
                msg, totalSize, freeSize, usedSize, maxHeapSizeInMB);
        d(c, line, tag, usage);
    }
    private static ArrayList<String> getMessage(Class<?> c, int line, String str) {
        if (c == null || str == null || str.isEmpty())
            return null;

        ArrayList<String> messages = new ArrayList<String>();
        String name = c.getName();
        int beginIndex = name.lastIndexOf(".");
        if (beginIndex != -1)
            name = name.substring(beginIndex + 1);

        str = name + "(" + line + "): " + str;
        int index = 0;
        final int maxLength = 2048; // substring for Chinese string counts 1 character as 2 bytes
        while (index < str.length()) {
            String sub;
            if (str.length() <= index + maxLength) {
                sub = str.substring(index);
            } else {
                sub = str.substring(index, index + maxLength);
            }

            messages.add(sub);
            index += maxLength;
        }
        return messages;
    }

    public static int getLineNumber() {
        return Thread.currentThread().getStackTrace()[2].getLineNumber();
    }

    public static void setDebug(boolean b) {
        DEBUG = b;
    }
}

