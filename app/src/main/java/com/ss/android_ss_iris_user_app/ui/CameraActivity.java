package com.ss.android_ss_iris_user_app.ui;

/**
 * Created by TitanGan on 2017/12/8.
 */

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.ss.android_ss_iris_user_app.Camera2BasicFragment;
import com.ss.android_ss_iris_user_app.R;

public class CameraActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);
        if (null == savedInstanceState) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, Camera2BasicFragment.newInstance())
                    .commit();
        }
    }

}
