package com.ss.android_ss_iris_user_app.ui;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by TitanGan on 2017/12/11.
 */

public class Util {
    static final public String FORMAT_FULL_DATETIME_SLASH = "yyyy/MM/dd HH:mm:ss";
    static final public String FORMAT_FULL_DATETIME = "yyyy-MM-dd HH:mm:ss";
    static final public String FORMAT_DATETIME = "yyyy-MM-dd HH:mm";
    static final public String FORMAT_SHORT_DATETIME = "yyyyMMddHHmm";
    static final public String FORMAT_SHORT_DATE = "yyyyMMdd";
    static final public String FORMAT_DATE = "yyyy-MM-dd";
    static final public String FORMAT_DATE_SLASH = "yyyy/MM/dd";
    static final public String FORMAT_TIME = "HH:mm";

    static protected ProgressDialog progressDialog = null;

    static public void hideDefaultKeyboard(AppCompatActivity parent, EditText editText) {
        editText.clearFocus();
        InputMethodManager imm = (InputMethodManager) parent.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
    }

    static public void hideDefaultKeyboard(Activity parent, EditText editText) {
        editText.clearFocus();
        InputMethodManager imm = (InputMethodManager) parent.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
    }

    static public void showToast(AppCompatActivity parent, String msg) {
        Toast.makeText(parent, msg, Toast.LENGTH_SHORT).show();
    }

    static public void showToast(AppCompatActivity parent,int msgId) {
        showToast(parent, parent.getResources().getString(msgId));
    }

    public static String getNowString(String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        Calendar calendar = Calendar.getInstance();
        return sdf.format(calendar.getTime());
    }

    static public ProgressDialog showProgress(AppCompatActivity parent, int msgId) {
        if (progressDialog != null)
            return progressDialog;
        progressDialog = ProgressDialog.show(parent, "", parent.getResources().getString(msgId), true);
        return progressDialog;
    }

    static public ProgressDialog showProgress(Activity parent, int msgId) {
        if (progressDialog != null)
            return progressDialog;
        progressDialog = ProgressDialog.show(parent, "", parent.getResources().getString(msgId), true);
        return progressDialog;
    }

    static public void dismissProgress() {
        if (progressDialog == null)
            return;

        progressDialog.dismiss();
        progressDialog = null;
    }
}
