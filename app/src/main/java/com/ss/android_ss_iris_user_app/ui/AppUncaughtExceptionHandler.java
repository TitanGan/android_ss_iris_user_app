package com.ss.android_ss_iris_user_app.ui;

import android.app.Activity;
import android.util.Log;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;

/**
 * Created by TitanGan on 2017/12/19.
 */

public class AppUncaughtExceptionHandler implements java.lang.Thread.UncaughtExceptionHandler {
    private final Activity context;
    public AppUncaughtExceptionHandler(Activity context) {
        this.context = context;
    }

    public void uncaughtException(Thread thread, Throwable exception) {
        Writer writer = new StringWriter();
        PrintWriter printWriter = new PrintWriter(writer);
        exception.printStackTrace(printWriter);

        String log = "SS"+" App Error Log: "+"\n"+writer.toString();
        Log.d("AppUncaughtExceptionHandler", log);
    }
}
