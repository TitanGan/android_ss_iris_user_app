package com.ss.android_ss_iris_user_app.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.ss.android_ss_iris_user_app.R;

/**
 * Created by TitanGan on 2017/12/12.
 */

public abstract class BaseDialog {

    private Activity activity;
    private Context context;
    private AlertDialog.Builder builder;
    private AlertDialog dialog;
    private View view;
    private OnCompletionListener listener = null;

    public interface OnCompletionListener<T>{
        void onCompletion(T result);
    }

    public BaseDialog(Activity activity, Context context, OnCompletionListener listener) {
        builder = new AlertDialog.Builder(activity);
        this.activity = activity;
        this.context = context;
        this.listener = listener;
    }

    public AlertDialog show() {
        if (builder != null) {
            dialog = builder.show();
            styleDialog(dialog);
        }
        return dialog;
    }

    public void setTitle(int titleId) {
        builder.setTitle(titleId);
    }

    public void setTitle(String titleStr) {
        builder.setTitle(titleStr);
    }

    public void setTitle(TextView titleText) {
        builder.setCustomTitle(titleText);
    }

    public void setMessage(int msgId) {
        builder.setMessage(msgId);
    }

    public void setMessage(String msgStr) {
        builder.setMessage(msgStr);
    }

    public void setView(int layout) {
        LayoutInflater inflater = activity.getLayoutInflater();
        view = inflater.inflate(layout, null);
        builder.setView(view);
    }

    public void setDialogButton(int confirmId, int cancelId, DialogInterface.OnClickListener listener) {
        builder.setCancelable(false);
        if (cancelId != 0)
            builder.setPositiveButton(confirmId, listener);
        if (cancelId != 0)
            builder.setNegativeButton(cancelId, listener);
    }

    public AlertDialog getDialog() {
        return dialog;
    }

    public View getDialogView() {
        return view;
    }

    public Activity getActivity() {
        return activity;
    }

    public AlertDialog.Builder getBuilder() {
        return builder;
    }

    public OnCompletionListener getListener() {
        return listener;
    }

    public Context getContext() {
        return context;
    }

    public void dismiss() {
        dialog.dismiss();
    }

    public TextView getDialogTitle() {
        TextView title = (TextView) dialog.findViewById(android.R.id.title);
        return title;
    }

    public TextView getDialogMsg() {
        TextView msg = (TextView) dialog.findViewById(android.R.id.message);
        return msg;
    }

    public void styleDialog(AlertDialog dialog) {
        styleDialog(activity, dialog);

    }

    public void styleDialog(Activity activity, AlertDialog dialog) {
        if (dialog == null)
            return;

//        int dividerId = dialog.getContext().getResources().getIdentifier("android:id/titleDivider", null, null);
//        if (dividerId != 0) {
//            View divider = dialog.findViewById(dividerId);
//            if (divider != null)
//                divider.setBackgroundColor(activity.getResources().getColor(R.color.black));
//        }

//        int textViewId = dialog.getContext().getResources().getIdentifier("android:id/alertTitle", null, null);
//        if (textViewId != 0) {
//            TextView tv = (TextView) dialog.findViewById(textViewId);
//            if (tv != null) {
//                tv.setTextColor(activity.getResources().getColor(R.color.white));
//                tv.setBackgroundColor(activity.getResources().getColor(R.color.black));
//            }
//        }
//
//        int layoutId = dialog.getContext().getResources().getIdentifier("android:id/topPanel", null, null);
//        if (layoutId != 0) {
//            LinearLayout layout = (LinearLayout) dialog.findViewById(layoutId);
//            if (layout != null)
//                layout.setBackgroundColor(activity.getResources().getColor(R.color.black));
//        }

        Button buttonCancel = dialog.getButton(DialogInterface.BUTTON_NEGATIVE);
        if(buttonCancel != null) {
            buttonCancel.setBackgroundResource(R.drawable.dialog_button_selector);
            buttonCancel.setTextColor(activity.getResources().getColor(R.color.white));
            buttonCancel.setTextSize(20);
        }

        Button buttonOk = dialog.getButton(DialogInterface.BUTTON_POSITIVE);
        if(buttonOk != null) {
            buttonOk.setBackgroundResource(R.drawable.dialog_button_selector);
            buttonOk.setTextColor(activity.getResources().getColor(R.color.white));
            buttonOk.setTextSize(20);
        }
    }
}
