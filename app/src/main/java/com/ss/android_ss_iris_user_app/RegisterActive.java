package com.ss.android_ss_iris_user_app;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.ss.android_ss_iris_user_app.Dialog.InformationDialog;
import com.ss.android_ss_iris_user_app.comm.op.CommCompletionHandler;
import com.ss.android_ss_iris_user_app.comm.op.CommError;
import com.ss.android_ss_iris_user_app.generic.InspectUserDatabase;
import com.ss.android_ss_iris_user_app.model.RegisteredItem;
import com.ss.android_ss_iris_user_app.ui.BaseDialog;
import com.ss.android_ss_iris_user_app.ui.Util;

/**
 * Created by TitanGan on 2017/12/12.
 */

public class RegisterActive extends AppCompatActivity {

    private Toolbar toolbar;
    private TextView mConfirmText;
    private String count = "", password = "", name = "", company = "";
    private EditText mCountEdit, mPasswordEdit, mUserEdit;
    private AppCompatActivity appCompatActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        appCompatActivity = this;

        toolbar = (Toolbar) findViewById(R.id.ToolBar);
        toolbar.setTitle(getResources().getString(R.string.title_activity_song_shang));
        toolbar.setLogo(getResources().getDrawable(R.drawable.icon_logo));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(RegisterActive.this, MainActivity.class));
                finish();
            }
        });

        mCountEdit = (EditText) findViewById(R.id.count_edit);
        setEditListener(mCountEdit);
        mCountEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                count = s.toString();
                checkConfirm();
            }
        });

        mPasswordEdit = (EditText) findViewById(R.id.password_edit);
        setEditListener(mPasswordEdit);
        mPasswordEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                password = s.toString();
                checkConfirm();
            }
        });

        mUserEdit = (EditText) findViewById(R.id.user_name_edit);
        setEditListener(mUserEdit);
        mUserEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                name = s.toString();
                checkConfirm();
            }
        });

        final Spinner mComSpinner = (Spinner) findViewById(R.id.company_spinner);
        final ArrayAdapter<String> mAdapter = new ArrayAdapter<String>(this,
                R.layout.spinner_adapter_single, getResources().getStringArray(R.array.company_list));
        mComSpinner.setAdapter(mAdapter);
        mComSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                company = mAdapter.getItem(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ImageButton mIrisButton = (ImageButton) findViewById(R.id.iris_button);
        ImageButton mRfidButton = (ImageButton) findViewById(R.id.rfid_button);

        mConfirmText = (TextView) findViewById(R.id.confirm_button);
        mConfirmText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Util.hideDefaultKeyboard(appCompatActivity, mCountEdit);
                if (count.length() < 7 || count.length() > 11) {
                    mCountEdit.setError(getResources().getString(R.string.error_invalid_register_count));
                    mCountEdit.requestFocus();
                    mConfirmText.setEnabled(false);
                    return;
                } else if (!checkDraft()) {
                    mCountEdit.setError(getResources().getString(R.string.error_invalid_count_exit));
                    mCountEdit.requestFocus();
                    mConfirmText.setEnabled(false);
                    return;
                } else if (password.length() < 7 || password.length() > 11) {
                    mPasswordEdit.setError(getResources().getString(R.string.error_invalid_register_count));
                    mPasswordEdit.requestFocus();
                    mConfirmText.setEnabled(false);
                    return;
                } else if (name.isEmpty()) {
                    mUserEdit.setError(getResources().getString(R.string.error_invalid_table_null));
                    mUserEdit.requestFocus();
                    mConfirmText.setEnabled(false);
                    return;
                } else
                    doRegistered();
            }
        });

    }

    private void doRegistered() {
        Util.showProgress(this, R.string.progress_dialog_msg);
//        RegisteredItem registeredItem = new RegisteredItem(count, password, name, company, null, null);
//        InspectUserDatabase.saveUserDraft(registeredItem, count);
//        Util.showToast(this, "註冊完成");

        String[] task = {"register", name, count, password};
        BackgroundTask backgroundTask = new BackgroundTask(new CommCompletionHandler<String>() {
            @Override
            public boolean onDataReceived(CommError error, int progress) {
                return false;
            }

            @Override
            public boolean onDataSent(CommError error, int progress) {
                return false;
            }

            @Override
            public boolean onCompletion(CommError error, String result, String response) {
                Util.dismissProgress();
                if (response != null) {
                    String[] strings = response.split(",");
                    if (strings[0].equals("Y") && strings[1].equals("Y")){
                        Util.showToast(appCompatActivity, "註冊成功");
                        cleanData();
                    }
                    else {
                        Util.showToast(appCompatActivity, "註冊失敗");
                    }
                }
                return false;
            }
        });
        backgroundTask.execute(task);
    }

    private void checkConfirm() {
        if (count.isEmpty() || password.isEmpty() || name.isEmpty())
            mConfirmText.setEnabled(false);
        else
            mConfirmText.setEnabled(true);
    }

    private boolean checkDraft() {
        final String draft = InspectUserDatabase.getUserDraft(count);
        if (draft == null)
            return true;
        else
            return false;
    }

    private void setEditListener(EditText editText) {
        editText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                    Util.hideDefaultKeyboard(appCompatActivity, mCountEdit);
                    cleanError();
                    return true;
                }
                return false;
            }
        });
    }

    private void cleanError() {
        mCountEdit.setError(null);
        mPasswordEdit.setError(null);
        mUserEdit.setError(null);
    }

    private void cleanData(){
        mCountEdit.setText("");
        mPasswordEdit.setText("");
        mUserEdit.setText("");
        count = "";
        password = "";
        name = "";
    }

}