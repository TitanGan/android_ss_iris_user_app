package com.ss.android_ss_iris_user_app.Dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.ss.android_ss_iris_user_app.R;
import com.ss.android_ss_iris_user_app.model.RegisteredItem;
import com.ss.android_ss_iris_user_app.ui.BaseDialog;
import com.ss.android_ss_iris_user_app.ui.Util;

/**
 * Created by TitanGan on 2017/12/12.
 */

public class InformationDialog extends BaseDialog {
    private RegisteredItem registeredItem;
    public InformationDialog(Activity activity, Context context, RegisteredItem registeredItem, OnCompletionListener listener) {
        super(activity, context, listener);
        this.registeredItem = registeredItem;
    }

    @Override
    public AlertDialog show() {
        setView(R.layout.dialog_title_textview);
        View view = getDialogView();
        TextView titleText = (TextView)view.findViewById(R.id.title_text);
        titleText.setText(R.string.suss_login);
        final TextView msgText = (TextView) view.findViewById(R.id.msg_text);
        String msg = "員工帳號 :" + registeredItem.getEmployeeId() + "\n\n姓名 :" + registeredItem.getUserName() +
                "\n\n登入時間 :" + Util.getNowString(Util.FORMAT_FULL_DATETIME_SLASH);
        msgText.setText(msg);
        AlertDialog dialog = super.show();
        dialog.getButton(DialogInterface.BUTTON_POSITIVE).setEnabled(false);
        return dialog;
    }
}
