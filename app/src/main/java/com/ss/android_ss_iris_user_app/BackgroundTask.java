package com.ss.android_ss_iris_user_app;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.ss.android_ss_iris_user_app.comm.op.CommCompletionHandler;
import com.ss.android_ss_iris_user_app.ui.AesEncryptionUtil;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLDecoder;
import java.net.URLEncoder;

/**
 * Created by titan.kan on 2017/12/25.
 */

public class BackgroundTask extends AsyncTask<String, Integer, String> {
    private String dataResponse;
    CommCompletionHandler<String> completionHandler;

    public BackgroundTask(CommCompletionHandler<String> completionHandler) {
        this.completionHandler = completionHandler;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... params) {
        try {
            String urlRegister = "http://192.168.101.108/mytest/AndroidRegister.php";
            String urlLogin = "http://192.168.101.108/mytest/AndroidLogin.php";
            String task = params[0];
            if (task.equals("register")) {
                URL url = new URL(urlRegister);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                OutputStream outputStream = httpURLConnection.getOutputStream();
                OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream, "UTF-8");
                BufferedWriter bufferedWriter = new BufferedWriter(outputStreamWriter);
                String myData = URLEncoder.encode("identifier_name", "UTF-8") + "=" +
                        AesEncryptionUtil.encrypt(URLEncoder.encode(params[1], "UTF-8"), "625202f9149e061d", "5efd3f6060e20330") + "&" +
                URLEncoder.encode("identifier_user", "UTF-8") + "=" +
                        AesEncryptionUtil.encrypt(URLEncoder.encode(params[2], "UTF-8"), "625202f9149e061d", "5efd3f6060e20330") + "&" +
                URLEncoder.encode("identifier_password", "UTF-8") + "=" +
                        AesEncryptionUtil.encrypt(URLEncoder.encode(params[3], "UTF-8"), "625202f9149e061d", "5efd3f6060e20330");
                String sss =AesEncryptionUtil.decrypt("4A3BA3AF65248B9452E44FF851EC6FB153FEC80AC94A065426ECFA43A2271E82", "625202f9149e061d", "5efd3f6060e20330");
                bufferedWriter.write(myData);
                bufferedWriter.flush();
                bufferedWriter.close();
                InputStream inputStream = httpURLConnection.getInputStream();
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream, "UTF-8");
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String[] response = bufferedReader.readLine().split(",");
                String isConn = response[0] + ",";
                String data = response[1];
                dataResponse = isConn + AesEncryptionUtil.decrypt(data, "625202f9149e061d", "5efd3f6060e20330");
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
            }
            if (task.equals("login")){
                URL url = new URL(urlLogin);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                OutputStream outputStream = httpURLConnection.getOutputStream();
                OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream, "UTF-8");
                BufferedWriter bufferedWriter = new BufferedWriter(outputStreamWriter);
                String myData = URLEncoder.encode("identifier_user", "UTF-8") + "=" +
                        AesEncryptionUtil.encrypt(URLEncoder.encode(params[1], "UTF-8"), "625202f9149e061d", "5efd3f6060e20330") + "&" +
                        URLEncoder.encode("identifier_password", "UTF-8") + "=" +
                        AesEncryptionUtil.encrypt(URLEncoder.encode(params[2], "UTF-8"), "625202f9149e061d", "5efd3f6060e20330");
                bufferedWriter.write(myData);
                bufferedWriter.flush();
                bufferedWriter.close();
                InputStream inputStream = httpURLConnection.getInputStream();
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream, "UTF-8");
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
//                String inputLine = "";
//                while ((inputLine = bufferedReader.readLine()).equals("null")){
//                    dataResponse = inputLine;
//                }
//                dataResponse = bufferedReader.readLine();
                String[] response = bufferedReader.readLine().split(",");
                String isConn = response[0] + ",";
                String data = response[1];
                dataResponse = isConn + AesEncryptionUtil.decrypt(data, "625202f9149e061d", "5efd3f6060e20330");
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
            }
        } catch (Exception e) {
            Log.e("Comm", "Download failed: " + e.getMessage());
        }
        return null;
    }

    @Override
    protected void onProgressUpdate(Integer... progress) {
        if (completionHandler != null)
            completionHandler.onDataReceived(null, progress[0].intValue());
    }

    @Override
    protected void onPostExecute(String result) {
        if (completionHandler != null)
            completionHandler.onCompletion(null, result, dataResponse);
    }
}