package com.ss.android_ss_iris_user_app;


import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.ss.android_ss_iris_user_app.Dialog.InformationDialog;
import com.ss.android_ss_iris_user_app.comm.GetLoginHttpOperation;
import com.ss.android_ss_iris_user_app.comm.bk.GetControlHttpOperation;
import com.ss.android_ss_iris_user_app.comm.bk.GetPeopleListHttpOperation;
import com.ss.android_ss_iris_user_app.comm.op.CommCompletionHandler;
import com.ss.android_ss_iris_user_app.comm.op.CommError;
import com.ss.android_ss_iris_user_app.generic.InspectControlDoorDatabase;
import com.ss.android_ss_iris_user_app.generic.InspectPeopleListDatabase;
import com.ss.android_ss_iris_user_app.generic.InspectUserDatabase;
import com.ss.android_ss_iris_user_app.model.ControlDoorItem;
import com.ss.android_ss_iris_user_app.model.RegisteredItem;
import com.ss.android_ss_iris_user_app.module.CtrlDevModule;
import com.ss.android_ss_iris_user_app.ui.BaseDialog;
import com.ss.android_ss_iris_user_app.ui.Util;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.ss.android_ss_iris_user_app.ui.BaseCommOperation.getJsonString;

/**
 * Created by TitanGan on 2017/12/8.
 */

public class InterFragment extends Fragment implements View.OnClickListener, ActivityCompat.OnRequestPermissionsResultCallback{
    private MainActivity parent;
    private int COUNT_ERROR = 0, PASSWORD_ERROR = 1, ALL_RIGHT = 4;
    private AutoCompleteTextView mEmployeeIdView;
    private EditText mPasswordView;
    private View mProgressView;
    private View mLoginFormView;
    private RegisteredItem registeredItem;
    private ImageButton mLoginButton, mCleanButton;

    public static InterFragment newInstance(MainActivity parent) {
        InterFragment interFragment = new InterFragment();
        interFragment.setMainActivity(parent);
        return interFragment;
    }

    private void setMainActivity(MainActivity parent) {
        this.parent = parent;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_inter_main, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(final View view, Bundle savedInstanceState) {
        final Activity activity = getActivity();
        mEmployeeIdView = (AutoCompleteTextView) view.findViewById(R.id.employee_id);
        mEmployeeIdView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });
        mPasswordView = (EditText) view.findViewById(R.id.password);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });
        mLoginButton = (ImageButton) view.findViewById(R.id.sign_in_button);
        mLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Util.showProgress(activity, R.string.progress_dialog_msg);
                String count = mEmployeeIdView.getText().toString();
                String password = mPasswordView.getText().toString();
                queryPersonData(count, password);
            }
        });
        mCleanButton = (ImageButton) view.findViewById(R.id.clean_button);
        mCleanButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mEmployeeIdView.setText("");
                mEmployeeIdView.setError(null);
                mPasswordView.setText("");
                mPasswordView.setError(null);
                Util.hideDefaultKeyboard(activity, mPasswordView);
            }
        });
    }

    @Override
    public void onClick(View v) {

    }

    private void attemptLogin() {
        final Activity activity = getActivity();
        Util.hideDefaultKeyboard(activity, mPasswordView);
        // Reset errors.
        mEmployeeIdView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String count = mEmployeeIdView.getText().toString();
        String password = mPasswordView.getText().toString();

        if (TextUtils.isEmpty(count)){
            mEmployeeIdView.setError(getString(R.string.error_invalid_email));
            mEmployeeIdView.requestFocus();
        }
        else if (TextUtils.isEmpty(password)){
            mPasswordView.setError(getString(R.string.error_incorrect_password));
            mPasswordView.requestFocus();
        }

    }

    private void checkLogin(){
        final Activity activity = getActivity();
        Util.hideDefaultKeyboard(activity, mPasswordView);

        String count = mEmployeeIdView.getText().toString();
        String password = mPasswordView.getText().toString();

        if (checkDraft(count, password) == PASSWORD_ERROR) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            mPasswordView.requestFocus();
        }

        else if (checkDraft(count, password) == COUNT_ERROR) {
            mEmployeeIdView.setError(getString(R.string.error_field_required));
            mEmployeeIdView.requestFocus();
        }
        else if (checkDraft(count, password) ==ALL_RIGHT) {
            //Util.showProgress(activity, R.string.suss_login);
            final InformationDialog dialog = new InformationDialog(getActivity(), getContext(), registeredItem,
                    new BaseDialog.OnCompletionListener<String>() {
                        @Override
                        public void onCompletion(String result) {

                        }
                    });
            dialog.show();
            final CtrlDevModule ctlDev = new CtrlDevModule();
            ctlDev.ctlDevice(CtrlDevModule.ExDevice.DOOR, CtrlDevModule.ExDeviceBehavior.BEHA_ON);//Open the door via CtrlDevModule @Vince.Chen 2017.12.15
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    dialog.getDialog().dismiss();
                    ctlDev.ctlDevice(CtrlDevModule.ExDevice.DOOR, CtrlDevModule.ExDeviceBehavior.BEHA_OFF);//Close the door via CtrlDevModule @Vince.Chen 2017.12.15
                }
            }, 3000);
        }
    }

    private int checkDraft(String count, String password) {
        final String draft = InspectUserDatabase.getUserDraft(count);
        if (draft == null) {
            return COUNT_ERROR;
        }
        registeredItem = new RegisteredItem();
        InspectUserDatabase.parseJson(registeredItem, draft);
        if (registeredItem.getPassword().equals(password))
            return ALL_RIGHT;
        else
            return PASSWORD_ERROR;
    }

    private void queryPersonData(String count, final String password){
        Util.showProgress(parent, R.string.progress_dialog_msg);
        String[] task = {count, password};
        GetPeopleListHttpOperation getPeopleListHttpOperation = new GetPeopleListHttpOperation(new CommCompletionHandler<RegisteredItem>() {
            @Override
            public boolean onDataReceived(CommError error, int progress) {
                return false;
            }

            @Override
            public boolean onDataSent(CommError error, int progress) {
                return false;
            }

            @Override
            public boolean onCompletion(CommError error, RegisteredItem result, String response) {
                if (result != null) {
                    registeredItem = result;
                    queryControlDoorListData();
                }
                else {
                    Util.dismissProgress();
                    Util.showToast(parent, "帳號或密碼錯誤,請重新輸入");
                }
                return false;
            }
        });
        getPeopleListHttpOperation.execute(task);
    }

    private void queryControlDoorListData() {
        Util.showProgress(parent, R.string.progress_dialog_msg);
        GetControlHttpOperation getControlHttpOperation = new GetControlHttpOperation(new CommCompletionHandler<ArrayList<ControlDoorItem>>() {
            @Override
            public boolean onDataReceived(CommError error, int progress) {
                return false;
            }

            @Override
            public boolean onDataSent(CommError error, int progress) {
                return false;
            }

            @Override
            public boolean onCompletion(CommError error, ArrayList<ControlDoorItem> result, String response) {
                if (result != null) {
                    for (ControlDoorItem controlDoorItem : result){
                        String info = controlDoorItem.getEmployeeId() + controlDoorItem.getDoorId();
                        if (info.equals(registeredItem.getEmployeeId() + S.DOOR)){
                            final InformationDialog informationDialog = new InformationDialog(getActivity(), getContext(), registeredItem,
                                    new InformationDialog.OnCompletionListener() {
                                        @Override
                                        public void onCompletion(Object result) {

                                        }
                                    });
                            informationDialog.show();
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    informationDialog.dismiss();
                                }
                            }, 3000);
                            Util.dismissProgress();
                            return true;
                        }
                    }
                }
                Util.showToast(parent, "權限不符,請洽後台人員");
                Util.dismissProgress();
                return false;
            }
        });
        getControlHttpOperation.execute();
    }

    private void queryData(String userId, String password) {
        new GetLoginHttpOperation(S.httpCommunicationManager, userId, password,
                new CommCompletionHandler<RegisteredItem>() {
                    @Override
                    public boolean onDataReceived(CommError error, int progress) {
                        return true;
                    }

                    @Override
                    public boolean onDataSent(CommError error, int progress) {
                        return true;
                    }

                    @Override
                    public boolean onCompletion(CommError error, RegisteredItem result, String response) {
                        if (error != null) {
                            return true; // can't do anything with error
                        }
                        registeredItem = result;
                        final InformationDialog dialog = new InformationDialog(getActivity(), getContext(), registeredItem,
                                new BaseDialog.OnCompletionListener<String>() {
                                    @Override
                                    public void onCompletion(String result) {

                                    }
                                });
                        dialog.show();
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                dialog.getDialog().dismiss();
                            }
                        }, 3000);
                        return true;
                    }
                });
    }
}
