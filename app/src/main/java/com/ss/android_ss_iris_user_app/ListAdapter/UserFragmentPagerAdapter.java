package com.ss.android_ss_iris_user_app.ListAdapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.List;

/**
 * Created by TitanGan on 2017/12/8.
 */

public class UserFragmentPagerAdapter extends FragmentStatePagerAdapter {
    private List<Fragment> fragments;

    public UserFragmentPagerAdapter(FragmentManager fm, List<Fragment> fragments) {
        super(fm);
        this.fragments = fragments;
    }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }
}