package com.ss.android_ss_iris_user_app.generic;

import com.ss.android_ss_iris_user_app.S;
import com.ss.android_ss_iris_user_app.comm.op.BaseCommOperation;
import com.ss.android_ss_iris_user_app.model.ControlDoorItem;
import com.ss.android_ss_iris_user_app.model.UserData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by titan.kan on 2018/1/9.
 */

public class InspectControlDoorDatabase {
    public static String getDraft(String info) {
        try {
            UserData data = S.userData.fetchFirstByInfo(info);
            if (data != null)
                return data.value;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void clearDraft(String info) {
        S.userData.deleteByInfo(info);
    }

    public static boolean checkDraft(String info) {
        try {
            UserData data = S.userData.fetchFirstByInfo(info);
            if (data != null)
                return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static void saveDraft(ControlDoorItem controlDoorItem, String info) {
        try {
            clearDraft(info);
            UserData data = new UserData(0, info, getJson(controlDoorItem), S.KIND_CONSTRO);
            S.userData.insert(data);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getJson(ControlDoorItem controlDoorItem) {
        JSONObject jsonObj = new JSONObject();
        try {
            jsonObj.put("EMPLOYEE_ID", controlDoorItem.getEmployeeId());
            jsonObj.put("DOOR_ID", controlDoorItem.getDoorId());
            jsonObj.put("DATE", controlDoorItem.getDate());
        } catch (JSONException error) {
        }
        return jsonObj.toString();
    }

    public static void parseJson(ControlDoorItem controlDoorItem, String json) {
        if (json == null || json.isEmpty())
            return;

        try {
            JSONObject jsonValidObj = new JSONObject(json);
            controlDoorItem.setEmployeeId(BaseCommOperation.getJsonString(jsonValidObj, "EMPLOYEE_ID", null));
            controlDoorItem.setDoorId(BaseCommOperation.getJsonString(jsonValidObj, "DOOR_ID", null));
            controlDoorItem.setDate(BaseCommOperation.getJsonString(jsonValidObj, "DATE", null));


        } catch (JSONException error) {
        }
    }
    public static ArrayList<ControlDoorItem> parseJson(String json) {
        if (json == null || json.isEmpty())
            return null;

        try {
            JSONArray jsonArray = new JSONArray(json);
            if (jsonArray != null){
                ArrayList<ControlDoorItem> controlDoorItems = new ArrayList<ControlDoorItem>();
                for (int i = 0; i < jsonArray.length(); i++){
                    JSONObject jsonValidObj = jsonArray.getJSONObject(i);
                    ControlDoorItem controlDoorItem = new ControlDoorItem();
                    controlDoorItem.setEmployeeId(BaseCommOperation.getJsonString(jsonValidObj, "EMPLOYEE_ID", null));
                    controlDoorItem.setDoorId(BaseCommOperation.getJsonString(jsonValidObj, "DOOR_ID", null));
                    controlDoorItem.setDate(BaseCommOperation.getJsonString(jsonValidObj, "DATE", null));

                    controlDoorItems.add(controlDoorItem);
                }
                return controlDoorItems;
            }

        } catch (JSONException error) {
        }
        return null;
    }


}

