package com.ss.android_ss_iris_user_app.generic;

import com.ss.android_ss_iris_user_app.S;
import com.ss.android_ss_iris_user_app.model.RegisteredItem;
import com.ss.android_ss_iris_user_app.model.UserData;
import com.ss.android_ss_iris_user_app.ui.BaseCommOperation;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by TitanGan on 2017/12/11.
 */

public class InspectUserDatabase {

    public final static String DATA_KEY = "user_data_draft";

    public static String getUserDraft(String info) {
        try {
            UserData data = S.userData.fetchFirstByInfo(info);
            if (data != null)
                return data.value;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void clearUserDraft(String info) {
        S.userData.deleteByInfo(info);
    }

    public static boolean checkUserDraft(String info) {
        try {
            UserData data = S.userData.fetchFirstByInfo(info);
            if (data != null)
                return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static void saveUserDraft(RegisteredItem registeredItem, String info) {
        try {
            UserData data = new UserData(0, info, getJson(registeredItem), 0);
            S.userData.insert(data);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static String getJson(RegisteredItem registeredItem) {
        JSONObject jsonObj = new JSONObject();
        try {
            jsonObj.put("EMPLOYEE_ID", registeredItem.getEmployeeId());
            jsonObj.put("PASSWORD", registeredItem.getPassword());
            jsonObj.put("USER_NAME", registeredItem.getUserName());
            jsonObj.put("GENDER", registeredItem.getGender());
            jsonObj.put("COMPANY", registeredItem.getCompany());
            jsonObj.put("DEPARTMENT", registeredItem.getDepartment());
            jsonObj.put("RFID", registeredItem.getRfid());
            jsonObj.put("IRIS", registeredItem.getIris());
            jsonObj.put("FACE", registeredItem.getFace());

        } catch (JSONException error) {
        }
        return jsonObj.toString();
    }

    public static void parseJson(RegisteredItem registeredItem, String json) {
        if (json == null || json.isEmpty())
            return;

        try {
            JSONObject jsonValidObj = new JSONObject(json);
            registeredItem.setEmployeeId(BaseCommOperation.getJsonString(jsonValidObj, "EMPLOYEE_ID", null));
            registeredItem.setPassword(BaseCommOperation.getJsonString(jsonValidObj, "PASSWORD", null));
            registeredItem.setUserName(BaseCommOperation.getJsonString(jsonValidObj, "USER_NAME", null));
            registeredItem.setGender(BaseCommOperation.getJsonString(jsonValidObj, "GENDER", null));
            registeredItem.setDepartment(BaseCommOperation.getJsonString(jsonValidObj, "DEPARTMENT", null));
            registeredItem.setCompany(BaseCommOperation.getJsonString(jsonValidObj, "COMPANY", null));
            registeredItem.setRfid(BaseCommOperation.getJsonString(jsonValidObj, "RFID", null));
            registeredItem.setIris(BaseCommOperation.getJsonString(jsonValidObj, "IRIS", null));
            registeredItem.setFace(BaseCommOperation.getJsonString(jsonValidObj, "FACE", null));

        } catch (JSONException error) {
        }
    }
}
