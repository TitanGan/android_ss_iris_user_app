package com.ss.android_ss_iris_user_app.generic;

import com.ss.android_ss_iris_user_app.S;
import com.ss.android_ss_iris_user_app.comm.op.BaseCommOperation;
import com.ss.android_ss_iris_user_app.model.RegisteredItem;
import com.ss.android_ss_iris_user_app.model.UserData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by titan.kan on 2018/1/10.
 */

public class InspectPeopleListDatabase {

    public static String getDraft(String info) {
        try {
            UserData data = S.userData.fetchFirstByInfo(info);
            if (data != null)
                return data.value;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void clearDraft(String info) {
        S.userData.deleteByInfo(info);
    }

    public static boolean checkDraft(String info) {
        try {
            UserData data = S.userData.fetchFirstByInfo(info);
            if (data != null)
                return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static void saveDraft(RegisteredItem registeredItem, String info) {
        try {
            clearDraft(info);
            UserData data = new UserData(0, info, getJson(registeredItem), S.KIND_EMPLOYEE);
            S.userData.insert(data);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getJson(RegisteredItem registeredItem) {
        JSONObject jsonObj = new JSONObject();
        try {
            jsonObj.put("EMPLOYEE_ID", registeredItem.getEmployeeId());
            jsonObj.put("PASSWORD", registeredItem.getPassword());
            jsonObj.put("USER_NAME", registeredItem.getUserName());
            jsonObj.put("GENDER", registeredItem.getGender());
            jsonObj.put("COMPANY", registeredItem.getCompany());
            jsonObj.put("DEPARTMENT", registeredItem.getDepartment());
            jsonObj.put("RFID", registeredItem.getRfid());
            jsonObj.put("IRIS", registeredItem.getIris());
            jsonObj.put("FACE", registeredItem.getFace());

        } catch (JSONException error) {
        }
        return jsonObj.toString();
    }

    public static void parseJson(RegisteredItem registeredItem, String json) {
        if (json == null || json.isEmpty())
            return;

        try {
            JSONObject jsonValidObj = new JSONObject(json);
            registeredItem.setEmployeeId(BaseCommOperation.getJsonString(jsonValidObj, "EMPLOYEE_ID", null));
            registeredItem.setPassword(BaseCommOperation.getJsonString(jsonValidObj, "PASSWORD", null));
            registeredItem.setUserName(BaseCommOperation.getJsonString(jsonValidObj, "USER_NAME", null));
            registeredItem.setGender(BaseCommOperation.getJsonString(jsonValidObj, "GENDER", null));
            registeredItem.setDepartment(BaseCommOperation.getJsonString(jsonValidObj, "DEPARTMENT", null));
            registeredItem.setCompany(BaseCommOperation.getJsonString(jsonValidObj, "COMPANY", null));
            registeredItem.setRfid(BaseCommOperation.getJsonString(jsonValidObj, "RFID", null));
            registeredItem.setIris(BaseCommOperation.getJsonString(jsonValidObj, "IRIS", null));
            registeredItem.setFace(BaseCommOperation.getJsonString(jsonValidObj, "FACE", null));


        } catch (JSONException error) {
        }
    }

    public static ArrayList<RegisteredItem> parseJson(String json) {
        if (json == null || json.isEmpty())
            return null;

        try {
            JSONArray jsonArray = new JSONArray(json);
            if (jsonArray != null) {
                ArrayList<RegisteredItem> registeredItems = new ArrayList<RegisteredItem>();
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonValidObj = jsonArray.getJSONObject(i);
                    RegisteredItem registeredItem = new RegisteredItem();
                    registeredItem.setEmployeeId(BaseCommOperation.getJsonString(jsonValidObj, "EMPLOYEE_ID", null));
                    registeredItem.setPassword(BaseCommOperation.getJsonString(jsonValidObj, "PASSWORD", null));
                    registeredItem.setUserName(BaseCommOperation.getJsonString(jsonValidObj, "USER_NAME", null));
                    registeredItem.setGender(BaseCommOperation.getJsonString(jsonValidObj, "GENDER", null));
                    registeredItem.setDepartment(BaseCommOperation.getJsonString(jsonValidObj, "DEPARTMENT", null));
                    registeredItem.setCompany(BaseCommOperation.getJsonString(jsonValidObj, "COMPANY", null));
                    registeredItem.setRfid(BaseCommOperation.getJsonString(jsonValidObj, "RFID", null));
                    registeredItem.setIris(BaseCommOperation.getJsonString(jsonValidObj, "IRIS", null));
                    registeredItem.setFace(BaseCommOperation.getJsonString(jsonValidObj, "FACE", null));

                    registeredItems.add(registeredItem);
                }
                return registeredItems;
            }

        } catch (JSONException error) {
        }
        return null;
    }

    public static RegisteredItem parseJsonItem(String json) {
        if (json == null || json.isEmpty())
            return null;

        try {
            JSONArray jsonArray = new JSONArray(json);
            if (jsonArray != null) {
                JSONObject jsonValidObj = jsonArray.getJSONObject(0);
                RegisteredItem registeredItem = new RegisteredItem();
                registeredItem.setEmployeeId(BaseCommOperation.getJsonString(jsonValidObj, "EMPLOYEE_ID", null));
                registeredItem.setPassword(BaseCommOperation.getJsonString(jsonValidObj, "PASSWORD", null));
                registeredItem.setUserName(BaseCommOperation.getJsonString(jsonValidObj, "USER_NAME", null));
                registeredItem.setGender(BaseCommOperation.getJsonString(jsonValidObj, "GENDER", null));
                registeredItem.setDepartment(BaseCommOperation.getJsonString(jsonValidObj, "DEPARTMENT", null));
                registeredItem.setCompany(BaseCommOperation.getJsonString(jsonValidObj, "COMPANY", null));
                registeredItem.setRfid(BaseCommOperation.getJsonString(jsonValidObj, "RFID", null));
                registeredItem.setIris(BaseCommOperation.getJsonString(jsonValidObj, "IRIS", null));
                registeredItem.setFace(BaseCommOperation.getJsonString(jsonValidObj, "FACE", null));
                return registeredItem;
            }

        } catch (JSONException error) {
        }
        return null;
    }


}

