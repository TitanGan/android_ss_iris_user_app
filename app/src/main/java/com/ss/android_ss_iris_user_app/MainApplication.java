package com.ss.android_ss_iris_user_app;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;

import com.ss.android_ss_iris_user_app.comm.op.BaseHttpOperation;
import com.ss.android_ss_iris_user_app.comm.op.CommDatabase;
import com.ss.android_ss_iris_user_app.comm.op.HttpCommunicationManager;
import com.ss.android_ss_iris_user_app.module.UserDataModule;

import java.util.ArrayList;

/**
 * Created by TitanGan on 2017/12/11.
 */

public class MainApplication extends Application {
    private final static int DB_VERSION = 4;
    private final static String DB_USER_NAME = "ss_user.db";
    private final static String DB_COMM_NAME = "ss_comm.db";
    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        MainApplication.context = this;

        // setup SharedManager / Modules
        S.userData = new UserDataModule(context, DB_USER_NAME, null, DB_VERSION);

        S.httpCommunicationManager = new HttpCommunicationManager();
        S.commDatabase = new CommDatabase(context, DB_COMM_NAME, DB_VERSION);

        S.httpCommunicationManager.setCommDatabase(S.commDatabase, new HttpCommunicationManager.OnRestoreDatabaseListener() {
            @Override
            public void onRestoreDatabaseListener(ArrayList<BaseHttpOperation> httpOperations) {
                // TODO: add background http transactions here
            }
        });

    }

    public static Context getAppContext() {
        return MainApplication.context;
    }

}
