package com.ss.android_ss_iris_user_app.comm;

import android.util.Log;

import com.ss.android_ss_iris_user_app.comm.op.BaseHttpOperation;
import com.ss.android_ss_iris_user_app.comm.op.CommCompletionHandler;
import com.ss.android_ss_iris_user_app.comm.op.CommError;
import com.ss.android_ss_iris_user_app.comm.op.HttpCommunicationManager;
import com.ss.android_ss_iris_user_app.model.DismantleItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by TitanGan on 2017/12/19.
 */

public class GetAutomaticDismantleListHttpOperation extends BaseHttpOperation {
    public static final String op = "getDismantleList";

    // FIXME: change url to EVA url when WS is ready.
    public static final String urlDebug = "http://54.186.63.63:8080/test_web_service/api/exec?op=getDismantleList";
    public static final String urlPilot = "https://myb2bt.evaair.com/BNW_WHO_WS/WHO_AUTO_WS.asmx/WS13_UNPACKING_QRY_CODE";
    public static final String urlRelease = "http://192.168.101.108:57144/";

    CommCompletionHandler<DismantleItem> completionHandler;

    private DismantleItem dismantleItem;

    public GetAutomaticDismantleListHttpOperation(HttpCommunicationManager communicationManager) {
        super(communicationManager);
    }

    public GetAutomaticDismantleListHttpOperation(HttpCommunicationManager communicationManager,
                                                  CommCompletionHandler<DismantleItem> completionHandler) {
        super(communicationManager);
        this.isMust = false;
        this.completionHandler = completionHandler;

        url = urlRelease;

        sendHttpRequest(false);
    }

    public GetAutomaticDismantleListHttpOperation(String resp) {
        super(resp);
    }

    @Override
    public String getJsonRequest() {
        JSONObject jsonObj = new JSONObject();
        try {
            //jsonObj.put("DEVICE_TOKEN", S.auth != null ? S.auth.getDeviceToken() : "");
            jsonObj.put("ITEM_CODE", "1300198");
        } catch (JSONException error) {
        }
        return jsonObj.toString();
    }

    public void parseJsonResponse(String resp) {
        if (resp == null)
            return;

        try {
            JSONObject jsonValidObj = new JSONObject(resp);
            String productName = getJsonString(jsonValidObj, "TITLE", null);
            String posCode = getJsonString(jsonValidObj, "POS_CODE", null);
            String dueDate = getJsonString(jsonValidObj, "STORAGE_LIFE_FROM_WHSE", null);
            ArrayList<String> picUrls = new ArrayList<String>();
            if (!jsonValidObj.isNull("PIC_ARY")) {
                JSONArray jsonPicListArray = jsonValidObj.getJSONArray("PIC_ARY");
                if (jsonPicListArray != null)
                    picUrls = decodeArrayString(jsonPicListArray);
            }
            dismantleItem = new DismantleItem(productName, posCode, dueDate, picUrls);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCompletion(CommError error, String resp) {
        parseJsonResponse(resp);
        if (completionHandler != null)
            completionHandler.onCompletion(error, dismantleItem, resp);
        return true;
    }

    private ArrayList<String> decodeArrayString(JSONArray jsonArray) {
        ArrayList<String> strings = new ArrayList<String>();
        try {
            for (int i = 0; i < jsonArray.length(); i++) {
                String string = jsonArray.getString(i);
                strings.add(string);

            }
        } catch (JSONException error) {
        }
        return strings;
    }
}