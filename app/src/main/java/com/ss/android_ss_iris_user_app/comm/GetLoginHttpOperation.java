package com.ss.android_ss_iris_user_app.comm;

import com.ss.android_ss_iris_user_app.comm.op.BaseHttpOperation;
import com.ss.android_ss_iris_user_app.comm.op.CommCompletionHandler;
import com.ss.android_ss_iris_user_app.comm.op.CommError;
import com.ss.android_ss_iris_user_app.comm.op.HttpCommunicationManager;
import com.ss.android_ss_iris_user_app.model.RegisteredItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by titan.kan on 2017/12/29.
 */

public class GetLoginHttpOperation extends BaseHttpOperation {
    public static final String op = "getDismantleList";

    // FIXME: change url to EVA url when WS is ready.
    public static final String urlDebug = "http://54.186.63.63:8080/test_web_service/api/exec?op=getDismantleList";
    public static final String urlPilot = "https://myb2bt.evaair.com/BNW_WHO_WS/WHO_AUTO_WS.asmx/WS13_UNPACKING_QRY_CODE";
    public static final String urlRelease = "http://192.168.101.108:50579/WebService1.asmx?op=WS1_USER_LOGIN";

    CommCompletionHandler<RegisteredItem> completionHandler;

    private RegisteredItem registeredItem;
    private String userId;
    private String password;

    public GetLoginHttpOperation(HttpCommunicationManager communicationManager) {
        super(communicationManager);
    }

    public GetLoginHttpOperation(HttpCommunicationManager communicationManager, String userId, String password,
                                 CommCompletionHandler<RegisteredItem> completionHandler) {
        super(communicationManager);
        this.isMust = false;
        this.completionHandler = completionHandler;
        this.userId = userId;
        this.password = password;

        url = urlRelease;

        sendHttpRequest(false);
    }

    public GetLoginHttpOperation(String resp) {
        super(resp);
    }

    @Override
    public String getJsonRequest() {
        JSONObject jsonObj = new JSONObject();
        try {
            //jsonObj.put("DEVICE_TOKEN", S.auth != null ? S.auth.getDeviceToken() : "");
            jsonObj.put("USER_ID", userId);
            jsonObj.put("USER_PWD", password);
        } catch (JSONException error) {
        }
        return jsonObj.toString();
    }

    public void parseJsonResponse(String resp) {
        if (resp == null)
            return;

        try {
            JSONObject jsonValidObj = new JSONObject(resp);
            String userId = getJsonString(jsonValidObj, "USER_ID", null);
            String name = getJsonString(jsonValidObj, "NAME", null);
            registeredItem = new RegisteredItem(userId, null, name, null, null, null, null, null, null);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCompletion(CommError error, String resp) {
        parseJsonResponse(resp);
        if (completionHandler != null)
            completionHandler.onCompletion(error, registeredItem, resp);
        return true;
    }
}
