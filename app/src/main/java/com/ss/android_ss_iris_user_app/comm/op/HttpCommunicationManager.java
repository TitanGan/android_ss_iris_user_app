package com.ss.android_ss_iris_user_app.comm.op;

import android.app.Activity;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;

import com.ss.android_ss_iris_user_app.model.UploadFile;
import com.ss.android_ss_iris_user_app.ui.LogUtil;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.CoreConnectionPNames;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import static java.net.Proxy.Type.HTTP;

/**
 * Created by TitanGan on 2017/12/19.
 */

public class HttpCommunicationManager {
    public interface OnRestoreDatabaseListener {
        public void onRestoreDatabaseListener(ArrayList<BaseHttpOperation> httpOperations);
    }

    private final static String LOG_TAG = "HttpComm";
    private final static boolean PRINT_LOG = true;

    public static final int REQUEST_INTERVAL = 100;
    private static final int CONNECTION_TIMEOUT_TIME = 60000;
    private static final int SO_TIMEOUT_TIME = 60000;
    private static final int MAX_FILE = 1000000;

    public static final int ERR_NONE = 0;
    public static final int ERR_UNKNOW_HOST = 1;
    public static final int ERR_HTTP_CONNECTION = 2;
    public static final int ERR_HTTP_RESPONSE = 3;
    public static final int ERR_HTTP_USER_CANCELED = 4;

    private CommDatabase commDatabase = null;
    private AppCompatActivity activity = null;
    private ArrayList<BaseHttpOperation> httpOperations = new ArrayList<BaseHttpOperation>();
    private Thread operationThread = null;
    private boolean enable = true;
    private UploadEntity entity = null;

    public HttpCommunicationManager() {
        operationThread = new Thread(sendRequestRunnable);
        operationThread.start();
    }

    public void setCommDatabase(CommDatabase commDatabase, OnRestoreDatabaseListener listener) {
        this.commDatabase = commDatabase;
        if (commDatabase != null && listener != null)
            listener.onRestoreDatabaseListener(commDatabase.fetchAll());
    }

    public void setActivity(AppCompatActivity activity) {
        this.activity = activity;
    }

    public void processHttpOperation(BaseHttpOperation operation, boolean isUrgent) {
        if (operation == null)
            return;

        if (isUrgent)
            httpOperations.add(0, operation);
        else
            httpOperations.add(operation);

        if (operation.isMust())
            commDatabase.insert(operation);
    }

    public void removeHttpOperation(BaseHttpOperation operation) {
        if (operation != null)
            return;

        httpOperations.remove(operation);
    }

    public int getUploadingFileCount(String owner) {
        int cnt = 0;
        for (BaseHttpOperation operation : httpOperations) {
            String o = operation.getOwner();
            if (o != null && o.equals(owner))
                cnt++;
        }
        return cnt;
    }

    private Runnable sendRequestRunnable = new Runnable() {
        @Override
        public void run() {
            while (true) {
                try {
                    if (enable) {
                        processRequests();
                    }

                    Thread.sleep(REQUEST_INTERVAL);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    };

    synchronized private boolean processRequests() {
        boolean ret = true;
        while (httpOperations.size() > 0 && ret) {
            LogUtil.d(getClass(), LogUtil.getLineNumber(), "HttpComm", "request size: " + httpOperations.size());
            BaseHttpOperation operation = httpOperations.get(0);
            if (operation.isUploadingFile())
                ret = uploadFile(operation);
            else
                ret = processRequest(operation);
        }
        return true;
    }

    private boolean processRequest(final BaseHttpOperation operation) {
        boolean ret = true;
        HttpClient client = new DefaultHttpClient();
        String url = operation.getUrl();
        HttpPost httppost = new HttpPost(url);
        httppost.setHeader("User-Agent", System.getProperty("http.agent"));

        String appName = operation.getAppName();
        String deviceToken = operation.getDeviceToken();
        String userId = operation.getUserId();
        String company = operation.getCompany();
        if (appName != null)
            httppost.setHeader("APP_NAME", appName);
        if (deviceToken != null)
            httppost.setHeader("DEVICE_TOKEN", deviceToken);
        if (userId != null)
            httppost.setHeader("USER_ID", userId);
        if (company != null)
            httppost.setHeader("COMPANY", company);
        httppost.setHeader("DEVICE_ID", Build.SERIAL);

        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        String output = operation.getJsonRequest();
        LogUtil.d(getClass(), LogUtil.getLineNumber(), "HttpComm", "request: " + url + "\n" +
                "APP_NAME="+appName+ "\n" +
                "DEVICE_TOKEN="+deviceToken+ "\n" +
                "USER_ID="+userId+ "\n" +
                "COMPANY="+company+ "\n" +
                "DEVICE_ID="+Build.SERIAL+ "\n" +
                "InputContent="+output);
        output = encodeBase64(output);
        nameValuePairs.add(new BasicNameValuePair("InputContent", output));

        // Execute HTTP POST Request
        HttpResponse response;
        String result = null;
        try {
            entity = new UploadEntity(nameValuePairs, "UTF-8", new UploadEntity.HttpPostProgressHandler() {
                @Override
                public void onDataTransferred(long transferred) {
                    if (entity != null) {
                        long total = entity.getContentLength();
                        if (total != 0)
                            onDataSent(operation, null, (int) (transferred * 100 / total));
                    }
                }
            });
            httppost.setEntity(entity);


            HttpClient httpclient = new DefaultHttpClient();
            httpclient.getParams().setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, CONNECTION_TIMEOUT_TIME);
            httpclient.getParams().setParameter(CoreConnectionPNames.SO_TIMEOUT, SO_TIMEOUT_TIME);

            response = httpclient.execute(httppost);

            StatusLine statusLine = response.getStatusLine();
            HttpEntity entity = response.getEntity();
            InputStream is = entity.getContent();
            if(statusLine.getStatusCode() == HttpStatus.SC_OK) {
                long len = entity.getContentLength();
                ByteArrayOutputStream baos = new ByteArrayOutputStream();

                byte[] buf = new byte[1024];
                long read = 0;
                int n;
                while((n = is.read(buf)) != -1) {
                    baos.write(buf, 0, n);
                    read += n;
                    onDataReceived(operation, null, (int)(read*100/len));
                }
                result = parseXmlString(baos.toString()).trim();
                baos.close();
            }
            else{
                //Closes the connection.
                is.close();
                httpclient.getConnectionManager().shutdown();
                onCompletion(operation, new CommError(ERR_HTTP_RESPONSE, statusLine.getReasonPhrase()), null);
                throw new IOException(statusLine.getReasonPhrase());
            }

            if (result != null)
                result = decodeBase64(result);
            LogUtil.d(getClass(), LogUtil.getLineNumber(), "HttpComm", "response: " + url + "\n" + result);
            is.close();
            httpclient.getConnectionManager().shutdown();

            if (result == null) {
                onCompletion(operation, new CommError(ERR_HTTP_CONNECTION, null), result);
            }
            else {
                if (isValidJsonResponse(result))
                    onCompletion(operation, null, result);
                else {
                    ret = false;
                    onCompletion(operation, new CommError(ERR_HTTP_RESPONSE, getJsonResponseMsg(result)), result);
                }
            }
        }
        catch (java.net.UnknownHostException error) {
            onCompletion(operation, new CommError(ERR_UNKNOW_HOST, error.getMessage()), result);
            ret = false;
        }
        catch (IOException error) {
            onCompletion(operation, new CommError(ERR_HTTP_CONNECTION, error.getMessage()), result);
            ret = false;
        }

        if (!operation.isMust() || ret) {
            if (commDatabase != null)
                commDatabase.delete((int)operation.getDbId());
            httpOperations.remove(operation);
        }
        else {
            // move failed MUST operation to last one
            httpOperations.remove(operation);
            httpOperations.add(operation);
        }

        return ret;
    }

    private boolean uploadFile(final BaseHttpOperation operation) {
//        boolean ret = true;
//        String url = operation.getUrl();
//        UploadFile uploadFile = operation.getUploadFile();
//        final HttpPost httppost = new HttpPost(url);
//        httppost.setHeader("User-Agent", System.getProperty("http.agent"));
//
//        String appName = operation.getAppName();
//        String deviceToken = operation.getDeviceToken();
//        String userId = operation.getUserId();
//        String company = operation.getCompany();
//        if (appName != null)
//            httppost.setHeader("APP_NAME", appName);
//        if (deviceToken != null)
//            httppost.setHeader("DEVICE_TOKEN", deviceToken);
//        if (userId != null)
//            httppost.setHeader("USER_ID", userId);
//        if (company != null)
//            httppost.setHeader("COMPANY", company);
//        httppost.setHeader("DEVICE_ID", Build.SERIAL);
//
//        // Execute HTTP Post Request
//        HttpResponse response;
//        try {
//            List<NameValuePair> nameValuePairs = uploadFile.getNameValuePairs();
//            String baseFile = "";
//            for (NameValuePair n : nameValuePairs){
//                if (n.getName().equals("FILE"))
//                    baseFile = n.getValue();
//            }
//            double b = (double) baseFile.length() / MAX_FILE;
//            int amount = (int)Math.ceil(b);
//            for (int i = 0; i < amount; i++){
//                List<NameValuePair> newNameValuePairs = new ArrayList<NameValuePair>();
//                for (NameValuePair n : nameValuePairs){
//                    if (n.getName().equals("PROG_NAME"))
//                        newNameValuePairs.add(n);
//                    else if (n.getName().equals("OPERATION_ID"))
//                        newNameValuePairs.add(n);
//                    else if (n.getName().equals("CODE_CATEGORY"))
//                        newNameValuePairs.add(n);
//                    else if (n.getName().equals("CODE_ID"))
//                        newNameValuePairs.add(n);
//                    else if (n.getName().equals("REMARK_SEQUENCE"))
//                        newNameValuePairs.add(n);
//                    else if (n.getName().equals("PIC_SEQUENCE"))
//                        newNameValuePairs.add(n);
//                    else if (n.getName().equals("SIGN_ID"))
//                        newNameValuePairs.add(n);
//                }
//                newNameValuePairs.add(new BasicNameValuePair("FILE_BLOCK_AMOUNT", String.valueOf(amount)));
//                newNameValuePairs.add(new BasicNameValuePair("FILE_BLOCK_SEQUENCE", String.valueOf(i)));
//                newNameValuePairs.add(new BasicNameValuePair("FILE_LENGTH", String.valueOf(baseFile.length())));
//
//                NameValuePair NameValuePairFile;
//                if (i == amount - 1)
//                    NameValuePairFile = new BasicNameValuePair("FILE_BLOCK", baseFile.substring(i * MAX_FILE, baseFile.length()));
//                else
//                    NameValuePairFile = new BasicNameValuePair("FILE_BLOCK", baseFile.substring(i * MAX_FILE, ((i + 1) * MAX_FILE)));
//
//                newNameValuePairs.add(NameValuePairFile);
//
//                entity = new UploadEntity(newNameValuePairs, HTTP.UTF_8, new UploadEntity.HttpPostProgressHandler() {
//                    @Override
//                    public void onDataTransferred(long transferred) {
//                        if (entity != null) {
//                            long total = entity.getContentLength();
//                            if (total != 0)
//                                onDataSent(operation, null, (int) (transferred * 100 / total));
//                        }
//                    }
//                });
//                httppost.setEntity(entity);
//
//                HttpClient httpclient = new DefaultHttpClient();
//                httpclient.getParams().setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, CONNECTION_TIMEOUT_TIME);
//                httpclient.getParams().setParameter(CoreConnectionPNames.SO_TIMEOUT, SO_TIMEOUT_TIME);
//
//                if (PRINT_LOG) {
//                    LogUtil.d(getClass(), LogUtil.getLineNumber(), LOG_TAG, "request: " + url + "\n" +
//                            "APP_NAME="+appName+ "\n" +
//                            "DEVICE_TOKEN="+deviceToken+ "\n" +
//                            "USER_ID="+userId+ "\n" +
//                            "COMPANY="+company+ "\n" +
//                            "DEVICE_ID="+Build.SERIAL);
//                    for (NameValuePair p : newNameValuePairs) {
//                        LogUtil.d(getClass(), LogUtil.getLineNumber(), LOG_TAG, " => " + p.getName() + ": " + p.getValue());
//                    }
//                }
//
//                response = httpclient.execute(httppost);
//                int statusCode = response.getStatusLine().getStatusCode();
//
//                String result = parseXmlString(EntityUtils.toString(response.getEntity()));
//                if (PRINT_LOG)
//                    LogUtil.d(getClass(), LogUtil.getLineNumber(), LOG_TAG, "response: " + url + "\n" + result);
//                httpclient.getConnectionManager().shutdown();
//
//                if (statusCode == HttpStatus.SC_OK && result != null && !result.isEmpty()) {
//                    if (isValidJsonResponse(result))
//                        onCompletion(operation, null, result);
//                    else {
//                        ret = false;
//                        onCompletion(operation, new CommError(ERR_HTTP_RESPONSE, getJsonResponseMsg(result)), result);
//                    }
//                }
//                else {
//                    ret = false;
//                    onCompletion(operation, new CommError(ERR_HTTP_RESPONSE, "Get response error!"), null);
//                }
//
//            }
//        } catch (IOException error) {
//            onCompletion(operation, new CommError(ERR_UNKNOW_HOST, error.getMessage()), null);
//            error.printStackTrace();
//            return false;
//        }
//
//        if (!operation.isMust() || ret) {
//            if (commDatabase != null)
//                commDatabase.delete((int)operation.getDbId());
//            httpOperations.remove(operation);
//            uploadFile.deleteFile();
//        }
//
//        return true;

        boolean ret = true;
        String url = operation.getUrl();
        UploadFile uploadFile = operation.getUploadFile();
        final HttpPost httppost = new HttpPost(url);
        httppost.setHeader("User-Agent", System.getProperty("http.agent"));

        // Execute HTTP Post Request
        HttpResponse response;
        try {
            List<NameValuePair> nameValuePairs = uploadFile.getNameValuePairs();
            entity = new UploadEntity(nameValuePairs, "UTF-8", new UploadEntity.HttpPostProgressHandler() {
                @Override
                public void onDataTransferred(long transferred) {
                    if (entity != null) {
                        long total = entity.getContentLength();
                        if (total != 0)
                            onDataSent(operation, null, (int) (transferred * 100 / total));
                    }
                }
            });
            httppost.setEntity(entity);

            HttpClient httpclient = new DefaultHttpClient();
            httpclient.getParams().setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, CONNECTION_TIMEOUT_TIME);
            httpclient.getParams().setParameter(CoreConnectionPNames.SO_TIMEOUT, SO_TIMEOUT_TIME);

            if (PRINT_LOG) {
                LogUtil.d(getClass(), LogUtil.getLineNumber(), LOG_TAG, "request: " + url);
                for (NameValuePair p : nameValuePairs) {
                    LogUtil.d(getClass(), LogUtil.getLineNumber(), LOG_TAG, " => " + p.getName() + ": " + p.getValue());
                }
            }

            response = httpclient.execute(httppost);
            int statusCode = response.getStatusLine().getStatusCode();

            String result = parseXmlString(EntityUtils.toString(response.getEntity()));
            if (PRINT_LOG)
                LogUtil.d(getClass(), LogUtil.getLineNumber(), LOG_TAG, "response: " + url + "\n" + result);
            httpclient.getConnectionManager().shutdown();

            if (statusCode == HttpStatus.SC_OK && result != null && !result.isEmpty()) {
                if (isValidJsonResponse(result))
                    onCompletion(operation, null, result);
                else {
                    ret = false;
                    onCompletion(operation, new CommError(ERR_HTTP_RESPONSE, getJsonResponseMsg(result)), result);
                }
            }
            else {
                ret = false;
                onCompletion(operation, new CommError(ERR_HTTP_RESPONSE, "Get response error!"), null);
            }
        } catch (IOException error) {
            onCompletion(operation, new CommError(ERR_UNKNOW_HOST, error.getMessage()), null);
            error.printStackTrace();
            return false;
        }

        if (!operation.isMust() || ret) {
            if (commDatabase != null)
                commDatabase.delete((int)operation.getDbId());
            httpOperations.remove(operation);
            uploadFile.deleteFile();
        }

        return true;
    }

    private void onDataReceived(final BaseHttpOperation operation, final CommError error, final int progress) {
        if (activity != null) {
            activity.runOnUiThread(new Runnable() {
                public void run() {
                    operation.onDataReceived(error, progress);
                }
            });
        }
    }

    private void onDataSent(final BaseHttpOperation operation, final CommError error, final int progress) {
        if (activity != null) {
            activity.runOnUiThread(new Runnable() {
                public void run() {
                    operation.onDataSent(error, progress);
                }
            });
        }
    }

    private void onCompletion(final BaseHttpOperation operation, final CommError error, final String resp) {
        if (activity != null) {
            activity.runOnUiThread(new Runnable() {
                public void run() {
                    operation.onCompletion(error, resp);
                }
            });
        }
    }

    private String encodeBase64(String str) {
        if (str == null)
            return null;

        String encodedString = null;
        try {
            encodedString = Base64.encodeToString(str.getBytes(), Base64.DEFAULT);
        } catch (Exception e) {
        }
        return encodedString;
    }

    private String encodeFileBase64(String filename) {
        if (filename == null)
            return null;

        byte[] bytes = null;
        try {
            InputStream is = new FileInputStream(filename);
            byte[] buffer = new byte[8192];
            int bytesRead;
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            while ((bytesRead = is.read(buffer)) != -1) {
                output.write(buffer, 0, bytesRead);
            }
            bytes = output.toByteArray();
            is.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (bytes == null)
            return null;

        return Base64.encodeToString(bytes, Base64.DEFAULT);
    }

    private String decodeBase64(String str) {
        if (str == null)
            return null;

        if (str.startsWith("{\""))
            return str;

        byte[] decode = null;
        try {
            decode = Base64.decode(str, Base64.DEFAULT);
        } catch (Exception e) {
        }
        if (decode == null)
            return null;
        return new String(decode);
    }

    private String parseXmlString(String data) {
        String result = data;
        if (data.startsWith("<?xml")) { // enclose json in xml
            int start = data.indexOf("\">") + 2;
            int end = data.indexOf("</string>");
            result = data.substring(start, end);
        }
        return result;
    }

    private boolean isValidJsonResponse(String resp) {
        if (resp == null || resp.isEmpty())
            return false;

        try {
            JSONObject jsonObj = new JSONObject(resp);
            if (!jsonObj.isNull("IS_VALID")) {
                String isValid = jsonObj.getString("IS_VALID");
                return (isValid.equals("Y"));
            }
        } catch (JSONException error) {
        }
        return false;
    }

    private String getJsonResponseMsg(String resp) {
        if (resp == null || resp.isEmpty())
            return null;

        try {
            JSONObject jsonObj = new JSONObject(resp);
            if (!jsonObj.isNull("MSG")) {
                return jsonObj.getString("MSG");
            }
        } catch (JSONException error) {
        }
        return null;
    }
}
