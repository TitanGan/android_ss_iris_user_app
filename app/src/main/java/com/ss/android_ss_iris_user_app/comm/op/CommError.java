package com.ss.android_ss_iris_user_app.comm.op;

/**
 * Created by TitanGan on 2017/12/19.
 */

public class CommError {
    public int code;
    public String message;
    public CommError(int code, String message) {
        this.code = code;
        this.message = message;
    }
}
