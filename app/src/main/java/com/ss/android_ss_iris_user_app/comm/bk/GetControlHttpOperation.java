package com.ss.android_ss_iris_user_app.comm.bk;

import android.os.AsyncTask;
import android.util.Log;

import com.ss.android_ss_iris_user_app.S;
import com.ss.android_ss_iris_user_app.comm.op.CommCompletionHandler;
import com.ss.android_ss_iris_user_app.generic.InspectControlDoorDatabase;
import com.ss.android_ss_iris_user_app.model.ControlDoorItem;
import com.ss.android_ss_iris_user_app.ui.AesEncryptionUtil;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

/**
 * Created by titan.kan on 2018/1/9.
 */

public class GetControlHttpOperation extends AsyncTask<String, Integer, String> {
    private String dataResponse;
    private ArrayList<ControlDoorItem> controlDoorItems;
    CommCompletionHandler<ArrayList<ControlDoorItem>> completionHandler;

    public GetControlHttpOperation(CommCompletionHandler<ArrayList<ControlDoorItem>> completionHandler) {
        this.completionHandler = completionHandler;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... params) {
        try {
            String urlLogin = "http://192.168.101.108/mytest/ServerGetDoorControl.php";
            URL url = new URL(urlLogin);
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setDoInput(true);
            OutputStream outputStream = httpURLConnection.getOutputStream();
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream, "UTF-8");
            BufferedWriter bufferedWriter = new BufferedWriter(outputStreamWriter);
            String myData = URLEncoder.encode("identifier_user", "UTF-8") + "=" +
                    AesEncryptionUtil.encrypt(URLEncoder.encode("root", "UTF-8"), "625202f9149e061d", "5efd3f6060e20330") + "&" +
                    URLEncoder.encode("identifier_password", "UTF-8") + "=" +
                    AesEncryptionUtil.encrypt(URLEncoder.encode("hh781212", "UTF-8"), "625202f9149e061d", "5efd3f6060e20330");
            bufferedWriter.write(myData);
            bufferedWriter.flush();
            bufferedWriter.close();
            InputStream inputStream = httpURLConnection.getInputStream();
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream, "UTF-8");
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            String response = bufferedReader.readLine();
            dataResponse = AesEncryptionUtil.decrypt(response, "625202f9149e061d", "5efd3f6060e20330");
            if (!dataResponse.equals("N"))
                controlDoorItems = InspectControlDoorDatabase.parseJson(dataResponse);
            bufferedReader.close();
            inputStream.close();
            httpURLConnection.disconnect();

        } catch (Exception e) {
            Log.e("Comm", "Download failed: " + e.getMessage());
        }
        return null;
    }

    @Override
    protected void onProgressUpdate(Integer... progress) {
        if (completionHandler != null)
            completionHandler.onDataReceived(null, progress[0].intValue());
    }

    @Override
    protected void onPostExecute(String result) {
        if (completionHandler != null)
            completionHandler.onCompletion(null, controlDoorItems, dataResponse);
    }
}