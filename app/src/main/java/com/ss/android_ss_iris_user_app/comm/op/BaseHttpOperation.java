package com.ss.android_ss_iris_user_app.comm.op;

import com.ss.android_ss_iris_user_app.model.UploadFile;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by TitanGan on 2017/12/19.
 */

public class BaseHttpOperation extends BaseCommOperation {
    protected HttpCommunicationManager communicationManager;
    protected String url = null;
    protected String className = null;
    protected long dbId;
    protected boolean isMust = false;
    protected boolean isUploadingFile = false;
    protected String owner = null;

    // common data in HTTPS header
    protected String appName = "";
    protected String deviceToken = "";
    protected String userId = "";
    protected String company = "";

    public BaseHttpOperation() {
    }

    public BaseHttpOperation(HttpCommunicationManager communicationManager) {
        this.communicationManager = communicationManager;
    }

    public BaseHttpOperation(String resp) {
        this.resp = resp;
        parseJsonResponse(resp);
    }

    public void setCommonData(String appName, String deviceToken, String userId, String company) {
        this.appName = appName;
        this.deviceToken = deviceToken;
        this.userId = userId;
        this.company = company;
    }

    public void sendHttpRequest(boolean isUrgent) {
        String req = this.getJsonRequest();
        if (req == null && !isUploadingFile)
            return;

        if (communicationManager != null)
            communicationManager.processHttpOperation(this, isUrgent);
    }

    public String getAppName() {
        return appName;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public String getUserId() {
        return userId;
    }

    public String getCompany() {
        return company;
    }

    public String getUrl() {
        return this.url;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getClassName() {
        return this.className;
    }

    public void setDbId(long dbId) {
        this.dbId = dbId;
    }

    public long getDbId() {
        return this.dbId;
    }

    public boolean isMust() {
        return isMust;
    }

    public boolean isUploadingFile() {
        return isUploadingFile;
    }

    public UploadFile getUploadFile() {
        return null;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getOwner() {
        return this.owner;
    }

    /**
     * Callback when sendHttpRequest has been completed.
     *
     * @return  true if processes successfully, otherwise return false then communication manager will resend it.
     */
    public boolean onCompletion(CommError error, String resp) {
        return true;
    }

    public boolean onDataReceived(CommError error, int progress) {
        return true;
    }

    public boolean onDataSent(CommError error, int progress) {
        return true;
    }

    ////////////////////////////////////////
    // Utility
    public String genResponseJson(String op, boolean valid, String msg) {
        JSONObject jsonObj = new JSONObject();
        try {
            jsonObj.put(OP, op);
            jsonObj.put(IS_VALID, valid);
            jsonObj.put(MSG, msg);
        } catch (JSONException error) {
            return null;
        }
        return jsonObj.toString();
    }

    public String getRequestOp(String req) {
        if (req == null)
            return null;
        try {
            JSONObject json = new JSONObject(req);
            return getJsonString(json, OP, "");
        } catch (JSONException error) {
        }
        return null;
    }

    public boolean isValidResponse() {
        if (resp == null)
            return false;
        try {
            JSONObject json = new JSONObject(resp);
            return json.getBoolean(IS_VALID);
        } catch (JSONException error) {
        }
        return false;
    }

    public String getResponseMsg(String resp) {
        if (resp == null)
            return null;
        try {
            JSONObject json = new JSONObject(resp);
            return getJsonString(json, MSG, "");
        } catch (JSONException error) {
        }
        return null;
    }

    // FIELD definition
    protected static final String OP = "Op";
    protected static final String IS_VALID = "IS_VALID";
    protected static final String MSG = "MSG";
    protected static final String YES = "Y";
    protected static final String NO = "N";
}
