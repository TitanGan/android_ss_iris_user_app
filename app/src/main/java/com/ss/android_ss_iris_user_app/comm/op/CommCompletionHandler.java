package com.ss.android_ss_iris_user_app.comm.op;

/**
 * Created by TitanGan on 2017/12/19.
 */


public interface CommCompletionHandler<T> {
    boolean onDataReceived(CommError error, int progress);
    boolean onDataSent(CommError error, int progress);
    boolean onCompletion(CommError error, T result, String response);
}

