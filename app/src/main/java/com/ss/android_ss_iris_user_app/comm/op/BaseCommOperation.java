package com.ss.android_ss_iris_user_app.comm.op;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by TitanGan on 2017/12/19.
 */

public class BaseCommOperation {
    public static final String op = "";
    protected String req = null;
    protected String resp = null;

    public BaseCommOperation() {
    }

    public BaseCommOperation(String resp) {
        this.resp = resp;
        parseJsonResponse(resp);
    }

    public void setResponse(String resp) {
        this.resp = resp;
        parseJsonResponse(resp);
    }

    public String getResponse() {
        return this.resp;
    }

    public void setJsonRequest(String req) {
        this.req = req;
    }

    public String getJsonRequest() {
        return req;
    }

    public void parseJsonResponse(String resp) {
        return;
    }

    public static String getJsonString(JSONObject jsonObj, String key, String defaultValue) throws JSONException {
        if (!jsonObj.isNull(key))
            return jsonObj.getString(key);

        return defaultValue;
    }

    public static boolean getJsonBoolean(JSONObject jsonObj, String key, boolean defaultValue) throws JSONException {
        if (!jsonObj.isNull(key))
            return jsonObj.getBoolean(key);

        return defaultValue;
    }

    public static int getJsonInt(JSONObject jsonObj, String key, int defaultValue) throws JSONException {
        if (!jsonObj.isNull(key))
            return jsonObj.getInt(key);

        return defaultValue;
    }

    public static long getJsonLong(JSONObject jsonObj, String key, long defaultValue) throws JSONException {
        if (!jsonObj.isNull(key))
            return jsonObj.getLong(key);

        return defaultValue;
    }

    public static double getJsonDouble(JSONObject jsonObj, String key, double defaultValue) throws JSONException {
        if (!jsonObj.isNull(key))
            return jsonObj.getDouble(key);

        return defaultValue;
    }
}
