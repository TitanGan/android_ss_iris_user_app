package com.ss.android_ss_iris_user_app.comm.op;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;

import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.List;

/**
 * Created by TitanGan on 2017/12/19.
 */

public class UploadEntity extends UrlEncodedFormEntity {
    public interface HttpPostProgressHandler {
        void onDataTransferred(long transferred);
    }

    private final HttpPostProgressHandler handler;

    public UploadEntity(List<? extends NameValuePair> parameters, String encoding, HttpPostProgressHandler handler) throws UnsupportedEncodingException {
        super(parameters, encoding);
        this.handler = handler;
    }

    @Override
    public void writeTo(OutputStream outputStream) throws IOException {
        super.writeTo(new HttpPostOutputStream(outputStream, this.handler));
    }

    public static class HttpPostOutputStream extends FilterOutputStream {
        private final static int BUF_SIZE = 51200;
        private final HttpPostProgressHandler handler;
        private long sent;

        public HttpPostOutputStream(final OutputStream out, final HttpPostProgressHandler handler) {
            super(out);
            this.handler = handler;
            this.sent = 0;
        }

        public void write(byte[] b, int off, int len) throws IOException {
            int left = len;
            int idx = off;
            while(left > 0) {
                int n = left;
                if (n > BUF_SIZE)
                    n = BUF_SIZE;
                out.write(b, idx, n);
                left -= n;
                idx += n;
                sent += n;
                if( this.handler != null )
                    this.handler.onDataTransferred(sent);
            }
        }

        public void write(int b) throws IOException {
            out.write(b);
            this.sent++;
            if( this.handler != null )
                this.handler.onDataTransferred(this.sent);
        }
    }
}
