package com.ss.android_ss_iris_user_app.comm.op;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.ss.android_ss_iris_user_app.comm.op.BaseHttpOperation;

import java.util.ArrayList;

/**
 * Created by TitanGan on 2017/12/19.
 */

public class CommDatabase extends SQLiteOpenHelper {
    private static final String LOG_TAG = "CommDatabase";

    // db configuration0
    private final static String TABLE_NAME = "request";

    // db table column name
    private final static String KEY_ID = "_id";
    private final static String KEY_CLASS = "_class";
    private final static String KEY_REQ = "_req";

    // db table column index
    private final static int TABLE_COLUMN_ID = 0;
    private final static int TABLE_COLUMN_CLASS = 1;
    private final static int TABLE_COLUMN_REQ = 2;

    // clear and reset db for debuging
    private final static boolean RESET_DB = false;

    public CommDatabase(Context context, String dbName, int dbVersion) {
        super(context, dbName, null, dbVersion);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        final String sql = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "( " + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + KEY_CLASS + " TEXT NOT NULL, "
                + KEY_REQ + " TEXT NOT NULL"
                + ");";
        Log.d(LOG_TAG, "onCreate sql: " + sql);

        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        final String sql = "DROP TABLE " + TABLE_NAME;
        Log.d(LOG_TAG, "onUpgrade sql: " + sql);
        db.execSQL(sql);
        onCreate(db);
    }

    synchronized public long insert(BaseHttpOperation baseHttpOperation) {
        SQLiteDatabase db = getWritableDatabase();
        if (RESET_DB)
            onUpgrade(db, 0, 1);

        ContentValues values = new ContentValues();
        values.put(KEY_CLASS, baseHttpOperation.getClass().getName());
        if (baseHttpOperation.isUploadingFile())
            values.put(KEY_REQ, baseHttpOperation.getUploadFile().getPath());
        else
            values.put(KEY_REQ, baseHttpOperation.getJsonRequest());
        long rowId = db.insert(TABLE_NAME, null, values);
        db.close();
        return rowId;
    }

    synchronized public ArrayList<BaseHttpOperation> fetchAll() {
        SQLiteDatabase db = getReadableDatabase();
        Cursor c = db.query(TABLE_NAME, new String[] { KEY_ID, KEY_CLASS, KEY_REQ }, null, null, null, null, null);
        if (c == null || c.getCount() <= 0) {
            closeDb(db, c);
            return new ArrayList<BaseHttpOperation>();
        }

        c.moveToFirst();
        ArrayList<BaseHttpOperation> httpOperations = new ArrayList<BaseHttpOperation>();
        for (int i = 0; i < c.getCount(); i++) {
            BaseHttpOperation baseHttpOperation = new BaseHttpOperation();
            baseHttpOperation.setDbId(c.getInt(TABLE_COLUMN_ID));
            baseHttpOperation.setClassName(c.getString(TABLE_COLUMN_CLASS));
            baseHttpOperation.setJsonRequest(c.getString(TABLE_COLUMN_REQ));
            httpOperations.add(baseHttpOperation);
            c.moveToNext();
        }
        closeDb(db, c);
        return httpOperations;
    }

    synchronized public int deleteAll() {
        SQLiteDatabase db = getReadableDatabase();
        int ret = db.delete(TABLE_NAME, null, null);
        db.close();
        return ret;
    }

    synchronized public int delete(int id) {
        SQLiteDatabase db = getReadableDatabase();
        int ret = db.delete(TABLE_NAME, KEY_ID + "=" + id, null);
        db.close();
        return ret;
    }

    private void closeDb(SQLiteDatabase db, Cursor cursor) {
        if (cursor != null)
            cursor.close();

        if (db != null)
            db.close();
    }
}
