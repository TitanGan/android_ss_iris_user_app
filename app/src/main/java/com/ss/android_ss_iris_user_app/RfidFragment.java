package com.ss.android_ss_iris_user_app;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;

import com.ss.android_ss_iris_user_app.ListAdapter.CircleProgressBar;

/**
 * Created by TitanGan on 2017/12/8.
 */

public class RfidFragment extends Fragment{

    public static RfidFragment newInstance() {
        return new RfidFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_rfid_main, container, false);
        return rootView;

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

    }

}
