package com.ss.android_ss_iris_user_app.model;

/**
 * Created by TitanGan on 2017/12/11.
 */

public class UserData {
    public int id = 0;
    public int kind = 0;
    public String info = null;
    public String value = null;

    public UserData(int id, String info, String value, int kind) {
        this.id = id;
        this.info = info;
        this.value = value;
        this.kind = kind;
    }
}
