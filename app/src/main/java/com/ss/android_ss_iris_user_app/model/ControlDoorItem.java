package com.ss.android_ss_iris_user_app.model;

/**
 * Created by titan.kan on 2018/1/9.
 */

public class ControlDoorItem {
    private String employeeId;
    private String doorId;
    private String date;

    public ControlDoorItem(){

    }

    public ControlDoorItem(String employeeId, String doorId, String date){
        this.employeeId = employeeId;
        this.doorId = doorId;
        this.date = date;
    }

    public String getDoorId() {
        return doorId;
    }

    public String getDate() {
        return date;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setDoorId(String doorId) {
        this.doorId = doorId;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }
}
