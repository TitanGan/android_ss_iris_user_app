package com.ss.android_ss_iris_user_app.model;

import com.ss.android_ss_iris_user_app.ui.BaseCommOperation;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by TitanGan on 2017/12/11.
 */

public class RegisteredItem implements Serializable {
    private static final long serialVersionUID = -7060210544600464481L;

    private String employeeId;
    private String password;
    private String userName;
    private String gender;
    private String company;
    private String department;
    private String rfid;
    private String iris;
    private String face;

    public RegisteredItem(){

    }

    public RegisteredItem(String employeeId, String password, String userName, String gender,
                          String company, String department, String rfid, String iris, String face){
        this.employeeId = employeeId;
        this.password = password;
        this.userName = userName;
        this.company = company;
        this.department = department;
        this.rfid = rfid;
        this.iris = iris;
        this.gender = gender;
        this.face = face;
    }

    public String getCompany() {
        return company;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public String getPassword() {
        return password;
    }

    public String getUserName() {
        return userName;
    }

    public String getIris() {
        return iris;
    }

    public String getRfid() {
        return rfid;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public void setIris(String iris) {
        this.iris = iris;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setRfid(String rfid) {
        this.rfid = rfid;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getFace() {
        return face;
    }

    public String getGender() {
        return gender;
    }

    public void setFace(String face) {
        this.face = face;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getJasonObject(){
        JSONObject jsonObj = new JSONObject();
        try {
            jsonObj.put("EMPLOYEE_ID", employeeId);
            jsonObj.put("PASSWORD", password);
            jsonObj.put("USER_NAME", userName);
            jsonObj.put("COMPANY", company);
            jsonObj.put("RFID", rfid);
            jsonObj.put("IRIS", iris);

        } catch (JSONException error) {
        }
        return jsonObj.toString();
    }

    public RegisteredItem parseJson(String json) {
        if (json == null || json.isEmpty())
            return null;
        RegisteredItem registeredItem = null;
        try {
            registeredItem = new RegisteredItem();
            JSONObject jsonValidObj = new JSONObject(json);
            registeredItem.setEmployeeId(BaseCommOperation.getJsonString(jsonValidObj, "EMPLOYEE_ID", null));
            registeredItem.setPassword(BaseCommOperation.getJsonString(jsonValidObj, "PASSWORD", null));
            registeredItem.setUserName(BaseCommOperation.getJsonString(jsonValidObj, "USER_NAME", null));
            registeredItem.setCompany(BaseCommOperation.getJsonString(jsonValidObj, "COMPANY", null));
            registeredItem.setRfid(BaseCommOperation.getJsonString(jsonValidObj, "RFID", null));
            registeredItem.setIris(BaseCommOperation.getJsonString(jsonValidObj, "IRIS", null));

        } catch (JSONException error) {
        }
        return registeredItem;
    }
}
