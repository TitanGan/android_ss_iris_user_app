package com.ss.android_ss_iris_user_app.model;

import android.util.Base64;
import android.util.Log;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by TitanGan on 2017/12/19.
 */

public class UploadFile {
    private int kind;
    private Map<String, String> params = null;
    private String path;

    public UploadFile() {
    }

    public UploadFile(int kind, Map<String, String> params, String path) {
        this.kind = kind;
        this.params = params;
        this.path = path;
    }

    public void setKind(int kind) {
        this.kind = kind;
    }

    public int getKind() {
        return this.kind;
    }

    public int getKindResp() {
        return this.kind + 100;
    }

    public void setParams(Map<String, String> params) {
        this.params = params;
    }

    public Map<String, String> getParams() {
        return this.params;
    }

    public String getParamValue(String key) {
        if (params == null)
            return null;
        return params.get(key);
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getPath() {
        return this.path;
    }

    public List<NameValuePair> getNameValuePairs() {
        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        if (params != null && path != null) {
            Iterator iterator = params.entrySet().iterator();
            while (iterator.hasNext()) {
                Map.Entry<String, String> entry = (Map.Entry<String, String>)iterator.next();
                if (entry != null)
                    nameValuePairs.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
            }
            nameValuePairs.add(new BasicNameValuePair("FILE", getFileData()));
        }
        return nameValuePairs;
    }

    private String getFileData() {
//        if (path == null)
//            return null;
//
//        try {
//            File f = new File(path);
//            Log.d("UploadFile", "File path: " + f.getAbsolutePath());
//            byte[] bytes = FileUtils.readFileToByteArray(f);
//            return Base64.encodeToString(bytes, Base64.DEFAULT);
//        } catch (Exception e) {
//        }
        return null;
    }

    public void deleteFile() {
        if (path == null)
            return;

        File f = new File(path);
        f.deleteOnExit();
    }
}

