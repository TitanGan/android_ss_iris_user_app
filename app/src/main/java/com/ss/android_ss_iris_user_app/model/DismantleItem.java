package com.ss.android_ss_iris_user_app.model;

import java.util.ArrayList;

/**
 * Created by TitanGan on 2017/12/19.
 */

public class DismantleItem {
    private String code, product, pos, date;
    private ArrayList<String> picUrls;

    public DismantleItem(String product, String pos, String date, ArrayList<String> picUrls){
        this.product = product;
        this.pos = pos;
        this.date = date;
        this.picUrls = picUrls;
    }

    public String getCode(){
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getProduct(){
        return product;
    }

    public String getPos(){
        return pos;
    }

    public String getDate(){
        return date;
    }

    public ArrayList<String> getPicUrls(){
        return picUrls;
    }
}
